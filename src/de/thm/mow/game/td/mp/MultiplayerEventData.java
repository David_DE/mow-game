package de.thm.mow.game.td.mp;

import android.os.Parcel;
import android.os.Parcelable;

public class MultiplayerEventData implements Parcelable {
	private MultiplayerEventDataType mEventType;
	private int from;
	private int mData[]; 

	public MultiplayerEventData(MultiplayerEventDataType type, int from) {
		this.mEventType = type;
		this.from = from;
	}
	
	public MultiplayerEventDataType getEventType() {
		return mEventType;
	}
	
	public int getFrom() {
		return from;
	}

	public int[] getData() {
		return mData;
	}

	public void setData(int[] data) {
		mData = data;
	}

	public MultiplayerEventData(Parcel in) {
		this.from = in.readInt();
		this.mEventType = MultiplayerEventDataType.getInstance(in.readInt());
		this.mData = in.createIntArray();
	}

	@Override
	public int describeContents() {
		return mEventType.getType();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(from);
		dest.writeInt(mEventType.getType());
		dest.writeIntArray(mData);

	}

	public static final Parcelable.Creator<MultiplayerEventData> CREATOR = new Parcelable.Creator<MultiplayerEventData>() {
		public MultiplayerEventData createFromParcel(Parcel in) {
			return new MultiplayerEventData(in);
		}

		public MultiplayerEventData[] newArray(int size) {
			return new MultiplayerEventData[size];
		}
	};
	
	public enum MultiplayerEventDataType {
		EVENT_PLAYER_LIFE(1), EVENT_PLAYER_TOWER_BUILD(2), EVENT_PLAYER_TOWER_UPGRADE(3), 
		EVENT_PLAYER_SEND_UNIT(4), EVENT_PLAYER_TOWER_SELL(5),
		EVENT_GAME_START(6), EVENT_GAME_END(7);

		private final int type;

		private MultiplayerEventDataType(int type) {
			this.type = type;
		}

		public int getType() {
			return this.type;
		}

		public static MultiplayerEventDataType getInstance(int t) {
			switch (t) {
			case 1:
				return EVENT_PLAYER_LIFE; // 0 = lives
			case 2:
				return EVENT_PLAYER_TOWER_BUILD; // 0 = x, 1 = y, 2 = towertype
			case 3:
				return EVENT_PLAYER_TOWER_UPGRADE; // 0 = x, 1 = y
			case 4:
				return EVENT_PLAYER_SEND_UNIT; // 0 = unittype
			case 5:
				return EVENT_PLAYER_TOWER_SELL; // 0 = x, 1 = y
			case 6:
				return EVENT_GAME_START;
			case 7:
				return EVENT_GAME_END;
			default:
				return null;
			}

		}

	}

}
