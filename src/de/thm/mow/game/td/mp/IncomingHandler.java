package de.thm.mow.game.td.mp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.util.Log;
import de.thm.mow.game.td.framework.GameNetwork;
import de.thm.mow.game.td.framework.NetworkEvent;

/**
 * Handler of incoming messages from service.
 */
public class IncomingHandler extends Handler {
	private GameNetwork mNetwork;

	public IncomingHandler(GameNetwork network) {
		mNetwork = network;
	}

	/**
	 * Packs all messeges in MultiplayerEvents and sends them to the GameNetwork
	 * class via addMultiplayerEvent()
	 */
	@Override
	public void handleMessage(Message msg) {
		Bundle b = msg.getData();
		switch (msg.what) {
		case MOWSchnittstellenInterface.MESSAGE_2_CONNECTION:
			int playerCout = b.getInt(MOWSchnittstellenInterface.CONNECTION_PLAYERCOUNT);
			int id = b.getInt(MOWSchnittstellenInterface.CONNECTION_OWN_ID);
			String names = b.getString(MOWSchnittstellenInterface.CONNECTION_PLAYERNAMES);
			String activity = b.getString(MOWSchnittstellenInterface.CONNECTION_ACTIVITY);

			Log.d(GameNetwork.TAG, "Bundle: id: " + id + " / playerCount: " + playerCout + " / names: " + names + " / activity: " + activity);
			NetworkEvent networkEvent1 = new NetworkEvent(MOWSchnittstellenInterface.MESSAGE_2_CONNECTION, playerCout, id, names, activity);

			mNetwork.addNetworkEvent(networkEvent1);
			break;

		case MOWSchnittstellenInterface.MESSAGE_4_GET:
			int adressId = b.getInt(MOWSchnittstellenInterface.GET_ADDRESS);
			String type = b.getString(MOWSchnittstellenInterface.GET_TYPE);
			byte[] newdata = b.getByteArray(MOWSchnittstellenInterface.GET_DATA);
			
			Log.d(GameNetwork.TAG + "4", "MESSAGE_4_GET: " + type + " / " + adressId);
			Parcel par = Parcel.obtain();
			par.unmarshall(newdata, 0, newdata.length);
	        par.setDataPosition(0); // this is extremely important!
			MultiplayerEventData temp = MultiplayerEventData.CREATOR.createFromParcel(par);
			par.recycle();
			
			NetworkEvent networkEvent2 = new NetworkEvent(MOWSchnittstellenInterface.MESSAGE_4_GET, adressId, Integer.parseInt(type), temp);
			mNetwork.addNetworkEvent(networkEvent2);
			break;

		case MOWSchnittstellenInterface.MESSAGE_6_CLOSED:
			int closedById = b.getInt(MOWSchnittstellenInterface.CLOSED_BY);
			NetworkEvent networkEvent3 = new NetworkEvent(MOWSchnittstellenInterface.MESSAGE_6_CLOSED, closedById);
			mNetwork.addNetworkEvent(networkEvent3);
			break;

		default:
			super.handleMessage(msg);
		}
	}
}