package de.thm.mow.game.td.mp;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ClassChooserDialog extends DialogFragment implements DialogInterface.OnClickListener {

	// Interface zum Kommunzieren mit der aufrufenden Activity
	public interface ClassChooserDialogListener {
		public void setSelectedService(PackageInfo service);
	}

	private ClassChooserDialogListener mListener;

	private List<PackageInfo> availableServices;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (ClassChooserDialogListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement ClassChooserDialogListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose your service");

		// Sucht alle installierten Services die in den spezifizierten Packages
		// liegen
		// (es werden nur Services beachtet, deren Package mit "de.thm.mps"
		// beginnt)
		availableServices = new ArrayList<PackageInfo>();
		List<PackageInfo> servicePackages = getActivity().getPackageManager().getInstalledPackages(PackageManager.GET_SERVICES);
		for (PackageInfo pi : servicePackages) {
			if (pi.packageName.startsWith(MOWSchnittstellenInterface.SERVICE_PACKAGE)) {
				availableServices.add(pi);
			}
		}

		// Listeneinträge des Dialogs erzeugen
		CharSequence[] texts = new CharSequence[availableServices.size()];
		for (int i = 0; i < availableServices.size(); i++) {
			PackageInfo service = availableServices.get(i);
			texts[i] = service.applicationInfo.loadLabel(getActivity().getPackageManager()) + " (" + service.packageName + ")";
		}
		builder.setItems(texts, this);

		return builder.create();
	}

	public void onClick(DialogInterface dialog, int which) {
		mListener.setSelectedService(availableServices.get(which));
	}

}
