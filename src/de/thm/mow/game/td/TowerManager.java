package de.thm.mow.game.td;

import java.util.ArrayList;

import android.util.Log;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.model.Tower;
import de.thm.mow.game.td.model.Tower.TowerType;

/**
 * TowerManager draws, updates and deletes tower
 */
public class TowerManager {
	
	private final String TAG = TowerManager.class.getSimpleName();
	private AndroidGame mGame;
	private ArrayList<Tower> towers;
	private ArrayList<Tower> towersToRemove;
	
	private SpawnManager mSpawnManager;
	private ProjectileManager mProjectileManager;
	
	public TowerManager(AndroidGame mGame, SpawnManager spawnManager, ProjectileManager projectileManager, RessourceManager ressourceManager){
		this.mGame = mGame;
		mSpawnManager = spawnManager;
		towers = new ArrayList<Tower>();
		towersToRemove = new ArrayList<Tower>();
		
		mProjectileManager = projectileManager;
	}
	
	public void update(float deltaTime){
		for (Tower t: towers) {
			t.update(deltaTime);
		}
	}
	
	public void draw(float deltaTime){
		GameGraphics graphics = mGame.getGraphics();
		Screen s = mGame.getCurrentScreen();

		final int oX = s.offsetX;
		final int oY = s.offsetY;
		
		for (Tower t: towers) {
			switch (t.type) {
			case TOWER_MG_1:
				graphics.drawRotatedBitmap(Asset.towerbullet1, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_MG_2:
				graphics.drawRotatedBitmap(Asset.towerbullet2, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_MG_3:
				graphics.drawRotatedBitmap(Asset.towerbullet3, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_FLAME_1:
				graphics.drawRotatedBitmap(Asset.towerflame1, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_FLAME_2:
				graphics.drawRotatedBitmap(Asset.towerflame2, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_FLAME_3:
				graphics.drawRotatedBitmap(Asset.towerflame3, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_ICE_1:
				graphics.drawRotatedBitmap(Asset.towerice1, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_ICE_2:
				graphics.drawRotatedBitmap(Asset.towerice2, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_ICE_3:
				graphics.drawRotatedBitmap(Asset.towerice3, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_LIGHTNING_1:
				graphics.drawRotatedBitmap(Asset.towerlightning1, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_LIGHTNING_2:
				graphics.drawRotatedBitmap(Asset.towerlightning2, t.x - oX, t.y - oY, t.angle);
				break;
			case TOWER_LIGHTNING_3:
				graphics.drawRotatedBitmap(Asset.towerlightning3, t.x - oX, t.y - oY, t.angle);
				break;

			default:
				Log.d(TAG, "This should not have happened in TowerManager->draw()?!");
				break;
			}
		}
		if (!towersToRemove.isEmpty()) towers.removeAll(towersToRemove);
	}
	
	public int addTower(Tower t) {
		t.setUnitList(mSpawnManager.getUnits());
		t.setPm(mProjectileManager);
		towers.add(t);
		return towers.indexOf(t);
	}
	
	public void removeTower(Tower t){
		towersToRemove.add(t);
		
	}

	public void upgradeTower(int id) {
		boolean debug = false;
		for(Tower t : towers){
			if(t.getTowerId() == id){
				t.upgrade();
				Log.d("mpbug","upgrade: " + id + " ==" + t.getTowerId());
				debug = true;
			}
		}
		
		Log.d("mpbug", "size" + towers.size());
		
		if (!debug) Log.d("mpbug","!upgrade: " + id);
	}
	
	public TowerType getTowerType(int id) {
		TowerType tp = null;
		for(Tower t : towers){
			if(t.getTowerId() == id){
				tp = t.type;
			}
		}
		return tp;
	}
	
	public Tower getTower(int id){
		Tower tower = null;
		for(Tower t : towers){
			if(t.getTowerId() == id)
				tower= t;		
		}
		return tower;
	}
	
}
