package de.thm.mow.game.td;

import java.util.ArrayList;

import android.util.Log;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.model.Tower;
import de.thm.mow.game.td.model.Tower.TowerType;
import de.thm.mow.game.td.model.map.ConstructionGround;
import de.thm.mow.game.td.model.map.Road;
import de.thm.mow.game.td.model.map.Tile;
import de.thm.mow.game.td.model.map.Tile.TileType;
import de.thm.mow.game.td.model.map.Wasteland;

/**
 * MapManager loads all Maps and draws them on the screen
 */
public class MapManager {
	private final String TAG = this.getClass().getSimpleName();
	public static final int MAP_TILES_PER_ROW = 20;
	public static final int MAP_TILES_PER_COLUMN = 10; // 8;

	private int roadStartIndex;
	private int towerIdCounter = 0;
	
	private AndroidGame mGame;
	private TowerManager mTowerManager;
	private UiManager mUiManager;

	private ArrayList<Tile> maplist;
	
	private int mPlayerId;
	private int mMapId;
	
	public MapManager(AndroidGame game, TowerManager towerManager, UiManager uimanager, int playerId, int mapid) {
		mGame = game;
		mTowerManager = towerManager;
		mUiManager = uimanager;	
		mPlayerId = playerId;
		mMapId = mapid;
		
		maplist = new ArrayList<Tile>(MAP_TILES_PER_COLUMN * MAP_TILES_PER_ROW);
		
		final int multiPlayerOffset = mPlayerId*MAP_TILES_PER_ROW*Tile.TILE_WIDTH;

		String mapString = loadMap();

		for (int i = 0; i < MAP_TILES_PER_COLUMN; i++) {
			for (int j = 0; j < MAP_TILES_PER_ROW; j++) {
				char c = mapString.charAt(i * MAP_TILES_PER_ROW + j);

				switch (c) {
				case 'C':
					maplist.add(new ConstructionGround(j * Tile.TILE_WIDTH + multiPlayerOffset, i * Tile.TILE_WIDTH, TileType.CONSTRUCTIONGROUND_EMPTY));

					break;
				case 'R':
					maplist.add(new Road(j * Tile.TILE_WIDTH + multiPlayerOffset, i * Tile.TILE_WIDTH, TileType.ROAD_NORMAL));

					break;
				case 'S':
					Road s = new Road(j * Tile.TILE_WIDTH + multiPlayerOffset, i * Tile.TILE_WIDTH, TileType.ROAD_START);
					maplist.add(s);
					roadStartIndex = i * MAP_TILES_PER_ROW + j;

					break;
				case 'F':
					Road f = new Road(j * Tile.TILE_WIDTH + multiPlayerOffset, i * Tile.TILE_WIDTH, TileType.ROAD_END);
					maplist.add(f);

					break;
				case 'W':
					maplist.add(new Wasteland(j * Tile.TILE_WIDTH + multiPlayerOffset, i * Tile.TILE_WIDTH, TileType.WASTELAND));

					break;

				default:
					Log.e(TAG, "Char: " + c + " sollte hier nicht vorkommen?!", new Exception("Fehler beim generieren der Map in MapManager()"));
				}
			}
		}
		
		
	}

	public void update(float deltaTime) {

	}

	public void draw(float deltaTime) {
		GameGraphics graphics = mGame.getGraphics();
		Screen s = mGame.getCurrentScreen();

		final int oX = s.offsetX;
		final int oY = s.offsetY;
		
		for (Tile t : maplist) {
			switch (t.type) {
			case ROAD_START:
				graphics.drawBitmap(Asset.road_start, t.x - oX, t.y - oY);
				break;
			case ROAD_NORMAL:
				graphics.drawBitmap(Asset.road_normale, t.x - oX, t.y - oY);
				break;
			case ROAD_END:
				graphics.drawBitmap(Asset.road_finish, t.x - oX, t.y - oY);
				break;
			case WASTELAND:
				graphics.drawBitmap(Asset.wasteland, t.x - oX, t.y - oY);
				break;
			case CONSTRUCTIONGROUND_TOWER:
			case CONSTRUCTIONGROUND_EMPTY:
				graphics.drawBitmap(Asset.constructionground_empty, t.x - oX, t.y - oY);
				break;
			default:
				Log.d(TAG, "Default");
				break;
			}

		}

	}

	/**
	 * This function will load the map from the file system and return it as a
	 * ascii layout. <br>
	 * C - {@link ConstructionGround} <br>
	 * R - {@link Road} <br>
	 * W - {@link Wasteland} <br>
	 * S - {@link Road}, but start <br>
	 * F - {@link Road}, but finish <br>
	 * 
	 * @return The map layout as a string
	 */
	public String loadMap() {
//		int x = LevelSelectScreen.getSelectedMap();
		return Asset.maps[mMapId];
	}

	/**
	 * ArrayOutOfBounds if scale is wronge! Scaleing not included!
	 * 
	 * @param x coordinate of the click event
	 * @param y coordinate of the click event
	 */
	public void clickAt(float x, float y) {
		if (!mUiManager.isGameMenuShowing()) {
			Tile t = maplist.get(getTileIndex(x, y));
			
			if (t.type == TileType.CONSTRUCTIONGROUND_EMPTY) {
				mUiManager.showTowerBuildUi(t.x + Tile.TILE_WIDTH/2, t.y + Tile.TILE_WIDTH/2);
				
			} else if (t.type == TileType.CONSTRUCTIONGROUND_TOWER) {
				mUiManager.showTowerUpgradeUi(t.x + Tile.TILE_WIDTH/2, t.y + Tile.TILE_WIDTH/2);
			}
			else if(t.type == TileType.WASTELAND){
				//TODO ?
			}
		}
	}

	public void longClickAt(float x, float y) {
		Tile t = maplist.get(getTileIndex(x, y));

		if (t.type == TileType.CONSTRUCTIONGROUND_TOWER) {
			
		}
		else if(t.type == TileType.WASTELAND){
			
		}
	}
	
	public int getTileIndex(float x, float y) {
		final int tileX = (int) Math.ceil(x / Tile.TILE_WIDTH) - 1;
		final int tileY = (int) Math.ceil(y / Tile.TILE_WIDTH) - 1;
		
		return tileY * MAP_TILES_PER_ROW + tileX;
	}
	
	/**
	 * 
	 * @param x koords
	 * @param y koords
	 * @return The Id of the Tower in the Arraylist of the TowerManager (towerId)
	 */
	public int getBuildId(float x, float y) { //TODO: not needed?
		return ((ConstructionGround)maplist.get(getTileIndex(x, y))).buildingId;
	}
	public int getTowerId(float x, float y){
		return ((ConstructionGround)maplist.get(getTileIndex(x, y))).towerId;
	}

	/**
	 * This methode generates are list of road tiles in the right direction for the
	 * movement of the units. get(0) = start / get(size()-1) = finish.
	 * @return an array of road tiles;
	 */
	public ArrayList<Tile> getGeneratedPath() {
		ArrayList<Tile> path = new ArrayList<Tile>();
		final int size = maplist.size();

		Tile temp = maplist.get(roadStartIndex);
		Tile selected;
		Log.d(TAG, "start at: " + roadStartIndex);
		
		path.add(temp);

		int rindex = roadStartIndex;
		int newrindex = 0;

		boolean step = false;
		
		while (temp.type != TileType.ROAD_END) {
			step = false;
			
			if ((newrindex = rindex - MAP_TILES_PER_ROW) >= 0) { // NORD
				selected = maplist.get(newrindex);
				if ((selected.type == TileType.ROAD_NORMAL || selected.type == TileType.ROAD_END) && !path.contains(selected)) {
					temp = selected;
					rindex = newrindex;
					path.add(temp);
					step = true;
				}
			}
			if (!step && (newrindex = rindex + 1) < size) { // EAST
				selected = maplist.get(newrindex);
				if ((selected.type == TileType.ROAD_NORMAL || selected.type == TileType.ROAD_END) && !path.contains(selected)) {
					temp = selected;
					rindex = newrindex;
					path.add(temp);
					step = true;
				}
			}
			if (!step && (newrindex = rindex + MAP_TILES_PER_ROW) < size) { // SOUTH
				selected = maplist.get(newrindex);
				if ((selected.type == TileType.ROAD_NORMAL || selected.type == TileType.ROAD_END) && !path.contains(selected)) {
					temp = selected;
					rindex = newrindex;
					path.add(temp);
					step = true;
				}
			}
			if (!step && (newrindex = rindex - 1) >= 0) { // WEST
				selected = maplist.get(newrindex);
				if ((selected.type == TileType.ROAD_NORMAL || selected.type == TileType.ROAD_END) && !path.contains(selected)) {
					temp = selected;
					rindex = newrindex;
					path.add(temp);
				}
			}
		}

		return path;
	}

	public void buildTower(TowerType type, int tileIndex) {
		
		Tile t = maplist.get(tileIndex);
		int id = towerIdCounter;
		Log.d("mpbug_build","build: " + id);
		towerIdCounter++;
		Tower tower = new Tower(t.x, t.y, type, id);
		((ConstructionGround)t).towerId = id;
		((ConstructionGround)t).buildingId = mTowerManager.addTower(tower);
		t.type = TileType.CONSTRUCTIONGROUND_TOWER;
	}
	
	public void sellTower(Tower t, int tileIndex){
		Tile tile = maplist.get(tileIndex);
		mTowerManager.removeTower(t);
		((ConstructionGround)tile).towerId = -1;
		((ConstructionGround)tile).buildingId = -1;
		tile.type = TileType.CONSTRUCTIONGROUND_EMPTY;
	}
	
}
