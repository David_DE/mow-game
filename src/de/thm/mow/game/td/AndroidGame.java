package de.thm.mow.game.td;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.GameInput;
import de.thm.mow.game.td.framework.GameNetwork;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.mp.ClassChooserDialog.ClassChooserDialogListener;
import de.thm.mow.game.td.screens.LoadingScreen;
import de.thm.mow.game.td.screens.MainMenuScreen;

/**
 * AndroidGame is the main (and only) activity of the game. 
 *
 */
public class AndroidGame extends FragmentActivity implements ClassChooserDialogListener {
	private GameView mRenderer;
	private GameGraphics mGraphics;
	private GameInput mInput;
	private GameNetwork mNetwork;
	private String mMapFile = "maps.txt";
	
	/**
	 * Contains the current {@link Screen} object
	 */
	private Screen mScreen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		
		int frameWidth = 800;
		int frameHeight = 480;
		Bitmap frameBuffer = Bitmap.createBitmap(frameWidth, frameHeight, Config.RGB_565);

		float displayWidth = getScreenInfo("width");
		float displayHeight = getScreenInfo("height");
		float scaleX = displayWidth / (float) frameWidth;
		float scaleY = displayHeight / (float) frameHeight;
		
			
		mRenderer = new GameView(this, frameBuffer);
		mGraphics = new GameGraphics(frameBuffer);
		mInput = new GameInput(mRenderer, scaleX, scaleY);
		mNetwork = new GameNetwork(this, getApplicationContext(), getSupportFragmentManager());

		setContentView(mRenderer);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		mScreen = new LoadingScreen(this);
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mRenderer.resume();
		mScreen.resume();
	}

	@Override
	public void onPause() {
		mRenderer.pause();
		mScreen.pause();
		
		
		super.onPause();
	}
	
	@Override
	public void onDestroy() {
		mScreen.dispose();
		Asset.disposeAll();
		super.onDestroy();
	}
	
	public GameGraphics getGraphics() {
		return mGraphics;
	}
	
	public Screen getCurrentScreen() {
		return mScreen;
	}
	
	public void setScreen(Screen screen) {
		mScreen.pause();
		mScreen.dispose();
		screen.resume();
		screen.update(0);
		mScreen = screen;
		
	}
	
	/**
	 * @return {@link GameInput} object
	 */
	public GameInput getInput() {
		return mInput;
	}
	
	public GameNetwork getNetwork() {
		return mNetwork;
	}
	
	public Bitmap getBitmapFromAsset(String strName)
    {
        AssetManager assetManager = getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("assetsloading", "excp bei " + strName + ": " + e.toString());
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }
	
	public String[] getMaps() {

		AssetManager am = this.getAssets();
		
		ArrayList<String> temp = new ArrayList<String>();
		String[] maps = null;
		
		try {
			InputStream is = am.open(mMapFile);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			String row = null;
			
			while ((row = in.readLine()) != null) {
				Log.d("mapsloading", "Gelesene Zeile: " + row);
				temp.add(row);
			}
			
			maps = new String[temp.size()];
			
			for (int i=0; i<temp.size(); i++)
			{
				maps[i] = temp.get(i);
			}
			
			in.close();
			
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return maps;	
	}
	
	public float getScreenInfo(String info){

	    Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics metrics = new DisplayMetrics();
	    display.getMetrics(metrics);
	    float returnValue;

	    if (info.equals("width")) returnValue = metrics.widthPixels;
	    else if(info.equals("height")) returnValue = metrics.heightPixels;
	    else returnValue = 0;		    
	    return returnValue;
	}
	
	@Override
	public void setSelectedService(PackageInfo service) {
		Log.d(GameNetwork.TAG, "setSelectedService" + service.toString());
		mNetwork.setSelectedService(service);
	}
	
	/**
	 * Creates a toast on the ui thread and shows the string
	 * @param text - What string should be displayed on the toast
	 */
	public void createMessage(String text) {
		
		final String t = text;
		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(AndroidGame.this, t, Toast.LENGTH_LONG).show();
			}
		});
		
	}
	
	@Override
	/**
	 * This function returns the user to the main menu or finishes the activity if
	 * he is already in the main menu.
	 */
	public void onBackPressed () {
		if (mScreen.getClass() == MainMenuScreen.class ) {
			finish();
		} else {
			Log.d("back", "Back to main menu");
			setScreen(new MainMenuScreen(this));
		}
		
	}
}
