package de.thm.mow.game.td;

import android.util.Log;

public class RessourceManager {
	private int cash;
	private int lifes;
	private boolean isStillAlive;
	
	public RessourceManager(){
		cash = 500;
		lifes = 20;
		isStillAlive = true;
	}
	
	public String getStringCash(){
		return Integer.toString(cash);
	}
	
	public int getCash(){
		return cash;
	}
	
	public int getLifes(){
		return lifes;
	}
	
	public void addCash(int amount){
		cash += amount;		
	}
	
	
	public boolean removeCash(int amount){
		boolean bRetVal = false;
		
		if(cash - amount >= 0){
			cash = cash - amount;
			bRetVal = true;
		}
		Log.i("CASH", Integer.toString(cash));
		return bRetVal;
	}
	
	public boolean hasEnoughMoneyFor(int money) {
		return (cash - money >= 0);
	}
	
	public boolean removeLifes(int l){
		lifes-=l;
		
		isStillAlive = (lifes > 0);
		
		return !isStillAlive;
	}
	
	public boolean isStillAlive(){
		return isStillAlive;
	}	
	
}
