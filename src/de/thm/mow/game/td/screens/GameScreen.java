package de.thm.mow.game.td.screens;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;

import de.thm.mow.game.td.AndroidGame;
import de.thm.mow.game.td.MapManager;
import de.thm.mow.game.td.Player;
import de.thm.mow.game.td.PlayerManager;
import de.thm.mow.game.td.framework.NetworkEvent;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.framework.TouchEvent;
import de.thm.mow.game.td.framework.TouchEvent.TouchEventType;
import de.thm.mow.game.td.model.Tower;
import de.thm.mow.game.td.model.Tower.TowerType;
import de.thm.mow.game.td.model.Unit.UnitType;
import de.thm.mow.game.td.model.map.Tile;
import de.thm.mow.game.td.mp.MOWSchnittstellenInterface;
import de.thm.mow.game.td.mp.MultiplayerEventData;

public class GameScreen extends Screen {
	private AndroidGame mGame;
	private Player local;

	private PlayerManager mPlayerManager;

	/**
	 * True if the game is a multiplayer game.
	 * TODO: Change to playerCount for games with more than two players
	 */
	private boolean mMultiplayer;
	private boolean paused = false;
	
	private boolean endMessageSend = false;

	public GameScreen(AndroidGame game, boolean multiplayer, Bundle b) {
		super(game);
		mGame = game;
		mMultiplayer = multiplayer;

		mPlayerManager = new PlayerManager();
		
		int mapid = 0;
		if (b != null) {
			mapid = b.getInt(LevelSelectScreen.MAP_SELECTED);
		}

		local = new Player(mGame, Player.LOCAL_PLAYER, mapid);

		mPlayerManager.addPlayer(local);

		if (mMultiplayer) {
			int ownid = b.getInt("KEY_ID_OWN");
			int enemyid = b.getInt("KEY_ID_ENEMY");
			Log.d("MPGameplay", "Own id: " + ownid + " / enemy id: " + enemyid);
			
			local.setNetworkId(ownid);
			
			// add second player
			Player enemy = new Player(mGame, Player.LOCAL_PLAYER + 1, mapid);
			enemy.setNetworkId(enemyid);
			mPlayerManager.addPlayer(enemy);
		}

	}

	@Override
	public void update(float deltaTime) {
		ArrayList<TouchEvent> touchEvents = mGame.getInput().getTouchEvents();
		ArrayList<NetworkEvent> networkEvents = mGame.getNetwork().getNetworkEvents();

		if (!paused || mMultiplayer) { // TODO Other "game states"?
			deltaOffsetX = 0;
			deltaOffsetY = 0;

			for (TouchEvent te : touchEvents) {
				if (te.type == TouchEventType.SCROLL) {
					deltaOffsetX = (int) te.x;
					deltaOffsetY = (int) te.y;
				} else if (te.type == TouchEventType.TAP) {
					if (local.getUiManager().clickedAt(te.x + offsetX, te.y + offsetY)) {

					} else {
						local.getMapManager().clickAt(te.x + offsetX, te.y + offsetY);
					}

				} else if (te.type == TouchEventType.LONG_TAP) {
					if (local.getUiManager().longClickAt(te.x + offsetX, te.y + offsetY)) {

					} else {
						local.getMapManager().longClickAt(te.x + offsetX, te.y + offsetY);
					}
				}
			}

			if (mMultiplayer) {
				for (NetworkEvent me : networkEvents) {
					Log.d("MPGameplay", "network event in gamescreen: " + me.getMsgType());
					switch (me.getMsgType()) {
					case MOWSchnittstellenInterface.MESSAGE_4_GET:
						/*
						 * TODO Get data type do something depending on the type
						 * (unit send, tower build, life lost, etc)
						 */
						int from = me.getAdressId();
						Player playerWhoSendMessage = mPlayerManager.findPlayerByNetworkId(from);

						MultiplayerEventData data = me.getMultiPlayerEventData();
						int[] gameData = data.getData();
						
						Log.d("MPGameplay", "got mpevent: " + data.getEventType().toString());

						switch (data.getEventType()) {
						case EVENT_PLAYER_LIFE:
							Log.d("MPGameplay", "In " + local.getNetworkId() + " - " + gameData[0] + " Leben verloren von: " + from);
							playerWhoSendMessage.getRessourceManager().removeLifes(gameData[0]);
							break;
						case EVENT_PLAYER_SEND_UNIT:
							Log.d("local", "Enemy send unit " + gameData[0] + "! Adding unit to spawnManager!");
							local.getSpawnManager().addUnit(UnitType.getInstance(gameData[0]));
							break;
						case EVENT_PLAYER_TOWER_BUILD:
							TowerType type = TowerType.getInstance(gameData[1]);
							playerWhoSendMessage.getMapManager().buildTower(type, gameData[0]);
							break;
						case EVENT_PLAYER_TOWER_UPGRADE:
							playerWhoSendMessage.getTowerManager().upgradeTower(gameData[0]);
							Log.d("mpbug", "got EVENT_PLAYER_TOWER_UPGRADE");
							break;
						case EVENT_PLAYER_TOWER_SELL:
							Tower t = playerWhoSendMessage.getTowerManager().getTower(gameData[0]);
							playerWhoSendMessage.getMapManager().sellTower(t, gameData[1]);
							break;
						case EVENT_GAME_END:
							if (gameData[1] > 0) {
								mGame.createMessage("Player " + from + " has survived with " + gameData[1] +" lifes!");
							} else {
								mGame.createMessage("Player " + from + " has been defeated!");
							}
							local.getUiManager().disableSendMenu();
							break;

						default:
							break;
						}

						break;
					case MOWSchnittstellenInterface.MESSAGE_6_CLOSED:
						mGame.createMessage("Player left the game (id: " + me.getClosedById() + ")"); 
						local.getUiManager().showMPGameWonUi();
						
						break;

					default:
						break;
					}
				}
			}

			int mapWidth = MapManager.MAP_TILES_PER_ROW * Tile.TILE_WIDTH;
			int mapHeight = MapManager.MAP_TILES_PER_COLUMN * Tile.TILE_WIDTH;
			int mp = 0;
			if (mMultiplayer) mp = 1;
			int multiPlayerOffset = mp*mapWidth;
			
			offsetX += deltaOffsetX * deltaTime;
			if (offsetX < 0)
				offsetX = 0;
			if (offsetX > mapWidth - 800 + multiPlayerOffset)
				offsetX = mapWidth - 800 + multiPlayerOffset;
			
			offsetY += deltaOffsetY * deltaTime;
			if (offsetY < 0)
				offsetY = 0;
			if (offsetY > mapHeight - 480)
				offsetY = mapHeight - 480;

			mPlayerManager.update(deltaTime);

			if (mPlayerManager.whoIsDead() == 0) {
				if (mMultiplayer) {
					if (!endMessageSend) {
						mGame.getNetwork().sendEndGameMessage(local.getNetworkId(), local.getRessourceManager().getLifes());
						endMessageSend = true;
					}
					local.getUiManager().showMPGameLostUi();
				} else {
					local.getUiManager().showSPGameLostUi();
				}
				paused = true;
			} else if (mPlayerManager.whoIsDead() == 1) {
				local.getUiManager().showMPGameWonUi();
				paused = true;
			}
			
			if(mPlayerManager.whoHasWon() == 0){
				if(mMultiplayer){
					if (!endMessageSend) {
						mGame.getNetwork().sendEndGameMessage(local.getNetworkId(), local.getRessourceManager().getLifes());
						endMessageSend = true;
					}
					local.getUiManager().showMPGameWonUi();
				} else {
					local.getUiManager().showSPGameWonUi();
				}
				paused = true;
			}


		} else if (paused) {

			for (TouchEvent te : touchEvents) {
				if (te.type == TouchEventType.TAP) {
					local.getUiManager().clickedAt(te.x + offsetX, te.y + offsetY);
				}
			}

		}
	}

	@Override
	public void draw(float deltaTime) {
		mPlayerManager.draw(deltaTime);
	}

	@Override
	public void pause() {
		local.getUiManager().showGamePausedUi();
		paused = true;
	}

	@Override
	public void resume() {
		if (!local.getUiManager().isGamePausedUiShowing()) {
			paused = false;
		}
	}

	@Override
	public void dispose() {
		if (mMultiplayer) {
			mGame.getNetwork().sendCloseMessage();
			mGame.getNetwork().doUnbind();
		}

	}

}
