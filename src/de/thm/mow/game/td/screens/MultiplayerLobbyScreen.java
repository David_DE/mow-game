package de.thm.mow.game.td.screens;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import de.thm.mow.game.td.AndroidGame;
import de.thm.mow.game.td.Asset;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.GameNetwork;
import de.thm.mow.game.td.framework.NetworkEvent;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.framework.TouchEvent;
import de.thm.mow.game.td.framework.TouchEvent.TouchEventType;
import de.thm.mow.game.td.mp.MOWSchnittstellenInterface;

public class MultiplayerLobbyScreen extends Screen {
	private static final String TAG = "lobby";
	private AndroidGame mGame;
	private GameNetwork mNetwork;

	private int mPlayerCount;
	private int mOwnId;
	private String[] mPlayerNames;
	private String mServiceActivity;

	private boolean mInfosAvailable = false;
	private boolean firstStart = true;
	private boolean askedToStart = false;
	private boolean gameStarting = false;

	public MultiplayerLobbyScreen(AndroidGame game) {
		super(game);
		mGame = game;

		mNetwork = mGame.getNetwork();
		mNetwork.showChooser();

		mPlayerCount = 0;
		mPlayerNames = null;
		mOwnId = -1;
		mServiceActivity = "";

	}

	@Override
	public void update(float deltaTime) {
		ArrayList<NetworkEvent> networkEvents = mNetwork.getNetworkEvents();
		ArrayList<TouchEvent> touchEvents = mGame.getInput().getTouchEvents();

		for (TouchEvent te : touchEvents) { //TODO: !!!
			if (te.type == TouchEventType.TAP) {
				if (te.x > 673 && te.x < 743 && te.y < 94 && te.y > 27) { 
					if (mNetwork.isBound()) {
						Log.d(TAG, "refresh - sendRegistrationMessage()");
						mNetwork.sendRegistrationMessage();						
					} else {
						mGame.createMessage("Connection is not bound!");
						mNetwork.showChooser();
					}
				} else if (te.x < 270 && te.x > 55 && te.y < 440 && te.y > 380) {
					Log.d(TAG, "back to main menu - sendCloseMessage()");
					mNetwork.sendCloseMessage();
					mGame.setScreen(new MainMenuScreen(mGame));
				} else if (te.x > 290 && te.x < 507 && te.y < 440 && te.y > 380) {
					if (mPlayerCount < 2) {
						Log.d(TAG, "open location chooser");
						mNetwork.showChooser();
					}
				} else if (te.x > 527 && te.x < 734 && te.y < 440 && te.y > 380) {
					if (mNetwork.isBound()) {
						if (mPlayerCount > 1) {
							askedToStart = true;
							mNetwork.sendStartGameMessage(mOwnId);
						} else {
							mGame.createMessage("You need 2 players to start the game!");
						}
					} else {
						mGame.createMessage("Connection is not bound!");
					}
				}
			}
		}

		for (NetworkEvent me : networkEvents) {
			switch (me.getMsgType()) {
			case MOWSchnittstellenInterface.MESSAGE_2_CONNECTION:
				mPlayerCount = me.getPlayerCnt();
				mServiceActivity = me.getActivityName();
				mOwnId = me.getOwnId();
				mPlayerNames = me.getPlayerNames();
				Log.d(GameNetwork.TAG, "Infos: " + mPlayerCount + " - " + mServiceActivity + " - " + mOwnId + " - " + mPlayerNames.length);

				mInfosAvailable = true;

				if (firstStart) {

					Intent i = new Intent(mServiceActivity);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					mGame.startActivity(i);

					firstStart = false;
				}
				
				break;
			case MOWSchnittstellenInterface.MESSAGE_4_GET:
				switch (me.getMpType()) {
				case 6: //EVENT_GAME_START
					Log.d(TAG, "EVENT_START_GAME angekommen bei " + mOwnId);
					
					if (!askedToStart) {
						Log.d(TAG, "!askedToStart bei " + mOwnId);
						mNetwork.sendStartGameMessage(mOwnId);
					}
					
					Bundle b = new Bundle();
					b.putInt("KEY_ID_OWN", mOwnId);
					b.putInt("KEY_ID_ENEMY", me.getAdressId());
					b.putInt(LevelSelectScreen.MAP_SELECTED, 0);
					gameStarting = true;
					mGame.setScreen(new GameScreen(mGame, true, b));
					Log.d(TAG, "gogo bei " + mOwnId);
					break;

				default:
					break;
				}
				
				/*
				 * TODO Not needed?
				 */

				break;
			case MOWSchnittstellenInterface.MESSAGE_6_CLOSED:
				/*
				 * TODO: (not needed?) Close connection return to main menu
				 */
				if(mPlayerCount > 1) {
					mGame.createMessage("Player left the game (id: " + me.getClosedById() + ")"); 
					mNetwork.sendRegistrationMessage();
				}

				break;

			default:
				break;
			}
		}

		/*
		 * TODO: Get InputEvents -> handle button presses Refresh -> send
		 * Registration msg Start game (only possible when game "ready", player > 1) -> send data with game started Back to main menu -> if player >
		 * 1 -> send closeing msg
		 */

	}

	@Override
	public void draw(float deltaTime) {
		GameGraphics g = mGame.getGraphics();
		
		g.drawBitmap(Asset.lobby, 0, 0);
		
		if (mInfosAvailable) {
			g.drawTextStandard("" + mPlayerCount, 75, 138, Color.WHITE);

			for (int i = 0; i < mPlayerNames.length; i++) {
				g.drawTextStandard(mPlayerNames[i], 60, 200 + i * 50, Color.BLACK);
			}
		}
		
		
		if(!mNetwork.isBound() || (mPlayerCount == 0)) {
			g.drawBitmap(Asset.button_select_service, 292, 379);
			
			g.drawBitmap(Asset.button_start_disabled, 528, 379);
		} else if (mPlayerCount < 2) {
			g.drawBitmap(Asset.button_start_disabled, 528, 379);
		}

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
		Log.d(TAG, "in resume");
		if(!firstStart) {
			mNetwork.sendRegistrationMessage();
		}

	}

	@Override
	public void dispose() {
		Log.d(TAG, "in dispose");
		
		if (!gameStarting) {
			mNetwork.sendCloseMessage();
			mNetwork.doUnbind();
		}
		

	}

}
