package de.thm.mow.game.td.screens;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import de.thm.mow.game.td.AndroidGame;
import de.thm.mow.game.td.Asset;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.framework.TouchEvent;
import de.thm.mow.game.td.framework.TouchEvent.TouchEventType;

public class LevelSelectScreen extends Screen {

	private AndroidGame mGame;
	public static int mMap = 0;
	public static final String MAP_SELECTED = "MAP_SELECTED";
	
	public LevelSelectScreen(AndroidGame game) {
		super(game);
		mGame = game;
	}

	@Override
	public void update(float deltaTime) {
		ArrayList<TouchEvent> touchEvents = mGame.getInput().getTouchEvents();

		for (TouchEvent te : touchEvents) {
			if (te.type == TouchEventType.TAP) {
				if (te.x > 55 && te.x < 185 && te.y > 155 && te.y < 285) {
					Log.d("levelselect","map 1");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 0);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}	
				if (te.x > 195 && te.x < 325 && te.y > 155 && te.y < 285) {
					Log.d("levelselect","map 2");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 1);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 335 && te.x < 465 && te.y > 155 && te.y < 285) {
					Log.d("levelselect","map 3");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 2);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 475 && te.x < 605 && te.y > 155 && te.y < 285) {
					Log.d("levelselect","map 4");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 3);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 615 && te.x < 745 && te.y > 155 && te.y < 285) {
					Log.d("levelselect","map 5");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 4);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 55 && te.x < 185 && te.y > 295 && te.y < 425) {
					Log.d("levelselect","map 6");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 5);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 195 && te.x < 325 && te.y > 295 && te.y < 425) {
					Log.d("levelselect","map 7");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 6);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 335 && te.x < 465 && te.y > 295 && te.y < 425) {
					Log.d("levelselect","map 8");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 7);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 475 && te.x < 605 && te.y > 295 && te.y < 425) {
					Log.d("levelselect","map 9");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 8);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
				if (te.x > 615 && te.x < 745 && te.y > 295 && te.y < 425) {
					Log.d("levelselect","map 10");
					Bundle b = new Bundle();
					b.putInt(MAP_SELECTED, 9);
					mGame.setScreen(new GameScreen(mGame, false, b));
				}
			}			
		}	
	}

	@Override
	public void draw(float deltaTime) {
		GameGraphics g = mGame.getGraphics();	
		g.drawBitmap(Asset.levelselect, 0, 0);	
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	public static int getSelectedMap(){
		return mMap;
	}

}
