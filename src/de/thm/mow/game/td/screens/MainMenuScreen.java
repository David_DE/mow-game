package de.thm.mow.game.td.screens;

import java.util.ArrayList;

import android.graphics.Paint;
import android.util.Log;
import de.thm.mow.game.td.AndroidGame;
import de.thm.mow.game.td.Asset;
import de.thm.mow.game.td.framework.GameNetwork;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.framework.TouchEvent;
import de.thm.mow.game.td.framework.TouchEvent.TouchEventType;

public class MainMenuScreen extends Screen {
	private AndroidGame mGame;

	public MainMenuScreen(AndroidGame game) {
		super(game);
		mGame = game;	
	}

	@Override
	public void update(float deltaTime) {
		ArrayList<TouchEvent> input = mGame.getInput().getTouchEvents();
		
		for (TouchEvent e: input) {
			if (e.type == TouchEventType.TAP) {
				if (e.x > 100 && e.x < 300 && e.y > 160 && e.y < 410) {
					mGame.setScreen(new LevelSelectScreen(mGame));
				} else if (e.x > 500 && e.x < 700 && e.y > 160 && e.y < 410) {
					Log.d(GameNetwork.TAG, "Chooser wird gestartet!");
					mGame.setScreen(new MultiplayerLobbyScreen(mGame));
				}
			}
		}
	}

	@Override
	public void draw(float deltaTime) {
		mGame.getGraphics().drawBitmap(Asset.menu, 0, 0, new Paint());
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}
}