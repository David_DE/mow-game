package de.thm.mow.game.td.screens;

import android.graphics.Paint;
import android.os.AsyncTask;
import de.thm.mow.game.td.AndroidGame;
import de.thm.mow.game.td.Asset;
import de.thm.mow.game.td.framework.Screen;

public class LoadingScreen extends Screen {

	private AndroidGame mGame;
//	private ProgressDialog mProgressDialog;

	public LoadingScreen(AndroidGame game) {
		super(game);
		mGame = game;	
		
		new LoadingTask().execute();
	}

	@Override
	public void update(float deltaTime) {	

	}


	@Override
	public void draw(float deltaTime) {	
		Asset.loading = mGame.getBitmapFromAsset("loading.png");
		mGame.getGraphics().drawBitmap(Asset.loading, 0, 0, new Paint());
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
	
	}

	@Override
	public void dispose() {
	
	}
	

	private class LoadingTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			//Screens
			Asset.menu = mGame.getBitmapFromAsset("menue.png");
			Asset.lobby = mGame.getBitmapFromAsset("multiplayer-lobby.png");
			Asset.levelselect = mGame.getBitmapFromAsset("screen-level-select.png");
			
			//Tower
			Asset.towerbullet1 = mGame.getBitmapFromAsset("tower-bullet-1-64.png");
			Asset.towerbullet2 = mGame.getBitmapFromAsset("tower-bullet-2-64.png");
			Asset.towerbullet3 = mGame.getBitmapFromAsset("tower-bullet-3-64.png");
			Asset.towerflame1 = mGame.getBitmapFromAsset("tower-fire-1-64.png");
			Asset.towerflame2 = mGame.getBitmapFromAsset("tower-fire-2-64.png");
			Asset.towerflame3 = mGame.getBitmapFromAsset("tower-fire-3-64.png");
			Asset.towerlightning1 = mGame.getBitmapFromAsset("tower-flash-1-64.png");
			Asset.towerlightning2 = mGame.getBitmapFromAsset("tower-flash-2-64.png");
			Asset.towerlightning3 = mGame.getBitmapFromAsset("tower-flash-3-64.png");
			Asset.towerice1 = mGame.getBitmapFromAsset("tower-ice-1-64.png");
			Asset.towerice2 = mGame.getBitmapFromAsset("tower-ice-2-64.png");
			Asset.towerice3 = mGame.getBitmapFromAsset("tower-ice-3-64.png");
			
			//Tower disabled
			Asset.towerice1Disabled = mGame.getBitmapFromAsset("tower-ice-1-64-disabled.png");
			Asset.towerbullet1Disabled = mGame.getBitmapFromAsset("tower-bullet-1-64-disabled.png");
			Asset.towerlightning1Disabled = mGame.getBitmapFromAsset("tower-flash-1-64-disabled.png");
			Asset.towerflame1Disabled = mGame.getBitmapFromAsset("tower-fire-1-64-disabled.png");
			
			//Projectiles
			Asset.ammo_bullet = mGame.getBitmapFromAsset("ammo-tower-bullet-8.png");
			Asset.ammo_flame = mGame.getBitmapFromAsset("ammo-tower-fire-8.png");
			Asset.ammo_lightning = mGame.getBitmapFromAsset("ammo-tower-flash-8.png");
			Asset.ammo_ice = mGame.getBitmapFromAsset("ammo-tower-ice-8.png");
			
			//Tiles
			Asset.road_start = mGame.getBitmapFromAsset("tile_road_texture.png");
			Asset.road_finish = mGame.getBitmapFromAsset("tile_road_texture.png");
			Asset.road_normale = mGame.getBitmapFromAsset("tile_road_texture.png");
			Asset.constructionground_empty = mGame.getBitmapFromAsset("tile_constructionground_texture.png");
			Asset.wasteland = mGame.getBitmapFromAsset("tile_wasteland_texture.png");		
			
			//UI elements
			Asset.ui_circle_empty = mGame.getBitmapFromAsset("chooser-empty.png");
			Asset.ui_tower_info = mGame.getBitmapFromAsset("towerinfo-384-256.png");
			Asset.ui_general_info = mGame.getBitmapFromAsset("general-info-384-256.png");
			Asset.ui_icon_upgrade = mGame.getBitmapFromAsset("icon-upgrade.png");
			Asset.ui_icon_delete_sell = mGame.getBitmapFromAsset("icon-delete-sell.png");
			Asset.ui_icon_upgrade_disabled = mGame.getBitmapFromAsset("icon-upgrade-disabled.png");
			Asset.ressource_window = mGame.getBitmapFromAsset("ressource-window-64-320.png");
			Asset.ui_game_over = mGame.getBitmapFromAsset("game-over-600-400.png");
			Asset.ui_game_paused = mGame.getBitmapFromAsset("game-pause-600-400.png");
			Asset.ui_game_win = mGame.getBitmapFromAsset("game-win-600-400.png");
			
			//UI Elements Multiplayer
//			Asset.ui_multiplayer_ressource_local = mGame.getBitmapFromAsset("multiplayer-ressource-window-no-pause-64-256.png");
			Asset.ui_multiplayer_ressource_enemy = mGame.getBitmapFromAsset("multiplayer-ressource-enemy-64-256.png");
			Asset.ui_multiplayer_send_units = mGame.getBitmapFromAsset("multiplayer-menu-send-units-800-128.png");
			Asset.ui_multiplayer_game_win = mGame.getBitmapFromAsset("multiplayer-you-win-600-400.png");
			Asset.ui_multiplayer_game_lose = mGame.getBitmapFromAsset("multiplayer-you-lose-600-400.png");
			Asset.ui_multiplayer_game_survived = mGame.getBitmapFromAsset("multiplayer-you-survived-600-400.png");

			//Units
			Asset.unit_circle = mGame.getBitmapFromAsset("unit-circle-32.png");
			Asset.unit_star = mGame.getBitmapFromAsset("unit-star-32.png");
			Asset.unit_square = mGame.getBitmapFromAsset("unit-square-32.png");
			
			//Buttons		
			Asset.button_start_disabled = mGame.getBitmapFromAsset("button-start-disabled.png");
			Asset.button_select_service = mGame.getBitmapFromAsset("button-select-service.png");
			
			//Maps
			Asset.maps = mGame.getMaps();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
//			mProgressDialog.dismiss();
			mGame.setScreen(new MainMenuScreen(mGame));	
		}

		@Override
		protected void onPreExecute() {
//			mProgressDialog = ProgressDialog.show(mGame, "Loading", "Please wait...");
		}

	}
}
