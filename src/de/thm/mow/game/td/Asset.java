package de.thm.mow.game.td;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Static collection of all ressources of the game. They will be loaded in at the time of the loading screen.
 */
public class Asset {
	
	//Tower ressources
	public static Bitmap towerbullet1;
	public static Bitmap towerbullet1Disabled;
	public static Bitmap towerbullet2;
	public static Bitmap towerbullet3;
	public static Bitmap towerflame1;
	public static Bitmap towerflame1Disabled;
	public static Bitmap towerflame2;
	public static Bitmap towerflame3;
	public static Bitmap towerlightning1;
	public static Bitmap towerlightning1Disabled;
	public static Bitmap towerlightning2;
	public static Bitmap towerlightning3;
	public static Bitmap towerice1;
	public static Bitmap towerice1Disabled;
	public static Bitmap towerice2;
	public static Bitmap towerice3;
	
	//Ammo ressources
	public static Bitmap ammo_bullet;
	public static Bitmap ammo_flame;
	public static Bitmap ammo_lightning;
	public static Bitmap ammo_ice;
	
	//Unit ressources
	public static Bitmap unit_circle;
	public static Bitmap unit_square;
	public static Bitmap unit_star;
	
	//Tile ressources
	public static Bitmap road_start;
	public static Bitmap road_normale;
	public static Bitmap road_finish;
	public static Bitmap constructionground_empty;
//	public static Bitmap constructionground_tower;
	public static Bitmap wasteland;

	//Other ressources
	public static Bitmap loading;
	public static Bitmap menu;
	public static Bitmap lobby;
	public static Bitmap levelselect;
	public static Bitmap button_start_disabled;
	public static Bitmap button_select_service;

	//InGame Ui elements
	public static Bitmap ui_circle_empty;
	public static Bitmap ressource_window;
	public static Bitmap ui_tower_info;
	public static Bitmap ui_icon_upgrade;
	public static Bitmap ui_icon_upgrade_disabled;
	public static Bitmap ui_icon_delete_sell;
	public static Bitmap ui_general_info;
	
	public static Bitmap ui_game_over;
	public static Bitmap ui_game_paused;
	public static Bitmap ui_game_win;
	
	//Multiplayer Ui elements
//	public static Bitmap ui_multiplayer_ressource_local;
	public static Bitmap ui_multiplayer_ressource_enemy;
	public static Bitmap ui_multiplayer_send_units;
	public static Bitmap ui_multiplayer_game_win;
	public static Bitmap ui_multiplayer_game_lose;
	public static Bitmap ui_multiplayer_game_survived;

	
	public static String[] maps;


	public static void disposeAll() {
		Log.i(Asset.class.getSimpleName(), "disposeAll()");
		towerbullet1.recycle();
		towerbullet1Disabled.recycle();
		towerbullet2.recycle();
		towerbullet3.recycle();
		towerflame1.recycle();
		towerflame1Disabled.recycle();
		towerflame2.recycle();
		towerflame3.recycle();
		towerlightning1.recycle();
		towerlightning1Disabled.recycle();
		towerlightning2.recycle();
		towerlightning3.recycle();
		towerice1.recycle();
		towerice1Disabled.recycle();
		towerice2.recycle();
		towerice3.recycle();

		ammo_bullet.recycle();
		ammo_flame.recycle();
		ammo_ice.recycle();
		ammo_lightning.recycle();

		unit_circle.recycle();
		unit_square.recycle();
		unit_star.recycle();

		road_start.recycle();
		road_normale.recycle();
		road_finish.recycle();
		constructionground_empty.recycle();
//		constructionground_tower.recycle();
		wasteland.recycle();

		loading.recycle();  
		menu.recycle();
		lobby.recycle();
		button_start_disabled.recycle();
		button_select_service.recycle();

		ui_circle_empty.recycle();
		ressource_window.recycle();
		ui_tower_info.recycle();
		ui_icon_upgrade.recycle();
		ui_icon_upgrade_disabled.recycle();
		ui_icon_delete_sell.recycle();
		ui_general_info.recycle();

		ui_game_over.recycle();
		ui_game_paused.recycle();
		ui_game_win.recycle();

//		ui_multiplayer_ressource_local.recycle();
		ui_multiplayer_ressource_enemy.recycle();
		ui_multiplayer_send_units.recycle();
		ui_multiplayer_game_win.recycle();
		ui_multiplayer_game_lose.recycle();

	}
	
}


