package de.thm.mow.game.td;

import java.util.ArrayList;

import android.graphics.Color;
import android.util.Log;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.model.Unit;
import de.thm.mow.game.td.model.Unit.UnitType;
import de.thm.mow.game.td.model.Wave;
import de.thm.mow.game.td.model.map.Tile;


public class SpawnManager {
	
	private AndroidGame mGame;
	private RessourceManager mRessourceManager;
	private ArrayList<Unit> waves;
	private ArrayList<Unit> unitsToRemove;
	private ArrayList<Tile> mPath;
	private Unit unit;
	private Wave w;
		
	private int firstSpawn = 0;
	private int firstSpawnTime = 3000;	
	private int spawnLoad = 0;
	private int spawnTime = 500;
	private int waveLoad = 0;
	private int waveTime = 15000;
	
	private ArrayList<UnitType> waveTypes; 
	
	int unitCnt = 0;
	int waveIndex = 0;
	int waveNumber = 0;
	
	private boolean readyToSpawn = false;
	private boolean readyForFirstSpawn = false;
	private boolean readyForNextWave = true;
	private boolean allWavesPassed = false;
	private boolean levelPassed = false;
	
	float deltaTime;
	
	private Player mPlayer;
	
	public SpawnManager(AndroidGame mGame, RessourceManager ressourceManager, Player player){ 
		this.mGame = mGame;	
		mRessourceManager = ressourceManager;
		waves = new ArrayList<Unit>();
		unitsToRemove = new ArrayList<Unit>();
		waveTypes = new ArrayList<UnitType>();	
		mPlayer = player;
	}
	
	public void update(float deltaTime){
		if((firstSpawn += (1000/60)*deltaTime) > firstSpawnTime){
			firstSpawn =- firstSpawnTime;
			readyForFirstSpawn = true;			
		}
		if ((spawnLoad += (1000 / 60) * deltaTime) > spawnTime) {
			spawnLoad -= spawnTime;
			readyToSpawn = true;
		}
		if ((waveLoad += (1000 / 60) * deltaTime) > waveTime) {
			waveLoad -= waveTime;
			readyForNextWave = true;
		}			
		if(readyForNextWave){
			unitCnt = 0;
			waveIndex = 0;
			
			w = new Wave(waveNumber++);
			if (w.hasAnextWave()){
				waveTypes = w.getWave();
			}			
			readyForNextWave = false;
			allWavesPassed = !w.hasAnextWave();						
		}		
		if (readyForFirstSpawn && !allWavesPassed) {
			initWave();	
		}
		
		for (Unit u: waves) {
//			u.x += 1*deltaTime;
			u.update(deltaTime);
		}
	}
	
	public void draw(float deltaTime){
		GameGraphics graphics = mGame.getGraphics();
		Screen s = mGame.getCurrentScreen();
		
		final int oX = s.offsetX;
		final int oY = s.offsetY;

		for (Unit u: waves) {
			
			if(!u.hasFinished() && !u.isDead() ){				
			
				if(u.type == UnitType.SOLDIER){
					graphics.drawBitmap(Asset.unit_circle, u.x - oX, u.y - oY);
				}
				else if(u.type == UnitType.TANK){
					graphics.drawBitmap(Asset.unit_star, u.x - oX, u.y - oY);
				}
				else if(u.type== UnitType.BOMB){
					graphics.drawBitmap(Asset.unit_square, u.x - oX, u.y - oY);
				}
				//white background hp bar
				graphics.drawRect(u.x - oX, u.y - oY -16, u.x + 32 - oX, u.y - 8 - oY, Color.WHITE);

				//green hp bar which shows to current hp
				float hpBarLength = (32f / u.hp) * u.hp_current;
				graphics.drawRect(u.x - oX, u.y - oY -16, u.x + hpBarLength - oX, u.y - 8 - oY, Color.GREEN);
			}
			else if (u.isDead()){
				mRessourceManager.addCash(u.bounty);				
				unitsToRemove.add(u);
			} else if (u.hasFinished()){
				
				if (mPlayer.getPlayerId() == Player.LOCAL_PLAYER) {
					mRessourceManager.removeLifes(u.strength);				
					Log.d("MPGameplay", mPlayer.getPlayerId() + " --> sendLostLifeDataMessage(): -" + u.strength);
					mGame.getNetwork().sendLostLifeDataMessage(mPlayer.getNetworkId(), u.strength);
				}
				
				unitsToRemove.add(u);
			}
		}
		if(!unitsToRemove.isEmpty()) waves.removeAll(unitsToRemove);	
		if(allWavesPassed && waves.size() == 0) levelPassed = true;
		
	}
	public boolean levelPassed(){
		return levelPassed;
	}
	
	public ArrayList<Unit> getUnits() {
		return waves;
	}
	
	public void setPath(ArrayList<Tile> path) {
		mPath = path;
	}
	
	public void initWave() {
		if(readyToSpawn && unitCnt < waveTypes.size() && mRessourceManager.isStillAlive()){		
			
			if (w.hasAnextWave()){				
				unit = new Unit(mPath, waveTypes.get(waveIndex));					
				waves.add(unit);
				waveIndex++;
				readyToSpawn = false;
				unitCnt++;					
			} 
		
		}
	}
	public void addUnit(UnitType uT){
		
		Unit u = new Unit(mPath, uT);
		waves.add(0, u);		
	}
}