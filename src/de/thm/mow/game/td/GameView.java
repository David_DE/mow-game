package de.thm.mow.game.td;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import de.thm.mow.game.td.framework.Screen;

/**
 * GameView draws a frameBuffer {@link Bitmap} to the screen. This happens in the inside of a
 * {@link Thread} in which the current screen gets drawn and updated.
 */
public class GameView extends SurfaceView implements Runnable {

	private AndroidGame mGame;
	private SurfaceHolder surfaceHolder;
	private Bitmap frameBuffer;
	private Thread renderThread = null;
	private boolean isRunning = false;
	
	private long lastTime = 0, currentTime = 0;

	public GameView(Context context) {
		super(context);
	}

	/**
	 * @param game 
	 * @param frameBuffer - The {@link Bitmap} which will be drawn to the screen
	 */
	public GameView(AndroidGame game, Bitmap frameBuffer) {
		super(game);
		this.mGame = game;
		renderThread = new Thread(this);
		surfaceHolder = getHolder();
		this.frameBuffer = frameBuffer;

		
	}

	public void resume() {
		lastTime = System.currentTimeMillis();
		isRunning = true;
		renderThread = new Thread(this);
		renderThread.start();
	}

	/**
	 * Run Methode des Threads .......
	 */
	@Override
	public void run() {
		long TPS = 1000 / 60; // ms pro Frame
		float temp;

		
		Rect dstRect = new Rect();

		while (isRunning) {
			if (!surfaceHolder.getSurface().isValid())
				continue;

			currentTime = System.currentTimeMillis();

			temp = (currentTime - lastTime);
			lastTime = currentTime;
			temp /= TPS;
			
			Screen s = mGame.getCurrentScreen();
			s.update(temp);
			s.draw(temp);
			
			Canvas canvas = surfaceHolder.lockCanvas();
			canvas.getClipBounds(dstRect);
			canvas.drawBitmap(frameBuffer, null, dstRect, null);
			
			surfaceHolder.unlockCanvasAndPost(canvas);

		}
	}
	
	public void pause() {
		isRunning = false;
		while (true) {
			try {
				renderThread.join();
				return;
			} catch (InterruptedException e) {	}
		}
	}
}
