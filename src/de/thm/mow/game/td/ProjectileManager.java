package de.thm.mow.game.td;

import java.util.ArrayList;

import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.model.Projectile;

/**
 * ProjectileManager draws, updates and deletes projectiles
 */
public class ProjectileManager {
	
	private AndroidGame mGame;
	private ArrayList<Projectile> projectiles;
	private ArrayList<Projectile> projectilesToRemove;
	
	public ProjectileManager(AndroidGame mGame){
		this.mGame = mGame;
		projectiles = new ArrayList<Projectile>();
		projectilesToRemove = new ArrayList<Projectile>();
	}
	
	public void update(float deltaTime){
		for (Projectile p: projectiles) {
			p.update(deltaTime);
			//Log.d("Projectile", "Anzahl Projektile: " + projectiles.size());
		}
		
		if(!projectilesToRemove.isEmpty()) projectiles.removeAll(projectilesToRemove);
	}
	
	public void draw(float deltaTime){
		GameGraphics graphics = mGame.getGraphics();
		Screen s = mGame.getCurrentScreen();
		
		final int oX = s.offsetX;
		final int oY = s.offsetY;
		
		for(Projectile p: projectiles){
			switch (p.type) {
			case AMMO_MG:
				graphics.drawBitmap(Asset.ammo_bullet, p.x - oX, p.y - oY);
				break;
			case AMMO_FLAME:
				graphics.drawBitmap(Asset.ammo_flame, p.x - oX, p.y - oY);
				break;
			case AMMO_ICE:
				graphics.drawBitmap(Asset.ammo_ice, p.x - oX, p.y - oY);
				break;
			case AMMO_LIGHTNING:
				graphics.drawBitmap(Asset.ammo_lightning, p.x - oX, p.y - oY);
				break;

			default:
				break;
			}
		}	
	}
	
	public int addProjectile(Projectile p) {
		projectiles.add(p);
		return projectiles.indexOf(p);
	}
	
	public void removeProjectile(Projectile p){
		projectiles.remove(p);
	}
	
	/**
	 * Adds a projectile to a list of projectiles which should be removed soon, 
	 * to prevent a ConcurrentModificationException
	 * 
	 * @param p Projectile
	 */
	public void projectilesToRemove(Projectile p){
		projectilesToRemove.add(p);
	}
}
