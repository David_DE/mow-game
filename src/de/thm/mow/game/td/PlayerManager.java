package de.thm.mow.game.td;

import java.util.ArrayList;

import de.thm.mow.game.td.framework.PlayerManagerCallback;
import de.thm.mow.game.td.model.Unit.UnitType;

public class PlayerManager implements PlayerManagerCallback {
	private ArrayList<Player> players;
	
	public PlayerManager() {
		players = new ArrayList<Player>();
	}
	
	
	public void draw(float deltaTime) {
		for (Player p: players) {
			p.draw(deltaTime);
		}

		/* The "local uis" are drawn last to ensure that the enemy life ui is
		 * behind possible local UIs the local
		 */
		for (int i = players.size() - 1; i >= 0; i--) {
			players.get(i).drawUi(deltaTime);
		}
	}
	
	public void update(float deltaTime) {
		for (Player p: players) {
			p.update(deltaTime);
		}
	}
	
	public void addPlayer(Player p) {
		p.addManagerCallback((PlayerManagerCallback)this);
		players.add(p);
		if(players.size()>1){	
			players.get(0).setMultiPlayer(true);	
		}
	}
	
	/**
	 * Local player will always be object 0 (Player.LOCAL_PLAYER)
	 * @return local player
	 */
	public Player getLocalPlayer() {
		return players.get(Player.LOCAL_PLAYER);
	}
	
	public Player findPlayerByNetworkId(int id) {
		for (Player p: players) {
			if (p.getNetworkId() == id) {
				return p;
			}
		}
		
		return null;
	}


	/**
	 * 
	 * @return -1 if nobody died yet or the local id of the dead player
	 */
	public int whoIsDead() {
		for (Player p: players) {
			if (!p.getRessourceManager().isStillAlive()) {
				return p.getPlayerId();
			}
		}
		return -1;
	}
	
	public int whoHasWon(){
		for (Player p: players){
			if(p.getSpawnManager().levelPassed()){				
				return p.getPlayerId();
			}		
		}
		return -1;
	}
	
	@Override
	public void sendUnitsToAllButLocal(UnitType type) {
		for (Player p: players) {
			if (p.getPlayerId() != Player.LOCAL_PLAYER) {
				p.getSpawnManager().addUnit(type);
			}
		}
	}

}
