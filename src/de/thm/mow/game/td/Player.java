package de.thm.mow.game.td;

import java.util.ArrayList;

import de.thm.mow.game.td.framework.PlayerManagerCallback;
import de.thm.mow.game.td.model.map.Tile;

public class Player {
	public final static int LOCAL_PLAYER = 0;

	private int mPlayerId;
	private int mNetworkId;
	private boolean mMultiPlayer;
	
	private int mMapId;
	
	private AndroidGame mGame;
	private MapManager mMapManager;
	private TowerManager mTowerManager;
	private UiManager mUiManager;
	private SpawnManager mSpawnManager;
	private ProjectileManager mProjectileManager;
	private RessourceManager mRessourceManager;
	
	private PlayerManagerCallback mManagerCallback;
	
	public Player(AndroidGame game, int id, int mapid) {
		mGame = game;
		mPlayerId = id;
		mMapId = mapid;
		
		mRessourceManager = new RessourceManager();

		mUiManager = new UiManager(mGame, mRessourceManager, this);
		mSpawnManager = new SpawnManager(mGame, mRessourceManager, this);
		mProjectileManager = new ProjectileManager(mGame);
		mTowerManager = new TowerManager(mGame, mSpawnManager, mProjectileManager, mRessourceManager);
		mMapManager = new MapManager(mGame, mTowerManager, mUiManager, mPlayerId, mMapId);
		
		ArrayList<Tile> path = mMapManager.getGeneratedPath();
		mSpawnManager.setPath(path);
	}
	
	
	public int getPlayerId() {
		return mPlayerId;
	}


	public void update(float deltaTime) {
		mTowerManager.update(deltaTime);
		mSpawnManager.update(deltaTime);
		mProjectileManager.update(deltaTime);
	}
	
	public void draw(float deltaTime) {
		mMapManager.draw(deltaTime); 
		mTowerManager.draw(deltaTime);
		mSpawnManager.draw(deltaTime);
		mProjectileManager.draw(deltaTime);	
	}
	
	public void drawUi(float deltaTime) {
		mUiManager.draw(deltaTime);		
	}
	
	public int getNetworkId() {
		return mNetworkId;
	}


	public void setNetworkId(int networkId) {
		mNetworkId = networkId;
	}
	
	public int getMapId() {
		return mMapId;
	}


	public MapManager getMapManager() {
		return mMapManager;
	}


	public TowerManager getTowerManager() {
		return mTowerManager;
	}


	public UiManager getUiManager() {
		return mUiManager;
	}


	public SpawnManager getSpawnManager() {
		return mSpawnManager;
	}


	public ProjectileManager getProjectileManager() {
		return mProjectileManager;
	}


	public RessourceManager getRessourceManager() {
		return mRessourceManager;
	}
	
	public PlayerManagerCallback getManagerCallback() {
		return mManagerCallback;
	}


	public void setMultiPlayer(boolean m) {
		mMultiPlayer = m;
		mUiManager.addMultiPlayerMenu();
	}
	
	public boolean isMultiplayer()
	{
		return mMultiPlayer;
	}


	public void addManagerCallback(PlayerManagerCallback playerManagerCallback) {
		mManagerCallback = playerManagerCallback;
		
	}
}
