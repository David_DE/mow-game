package de.thm.mow.game.td.model;

import java.util.ArrayList;

import de.thm.mow.game.td.model.map.Tile;

public class Path {

	private ArrayList<Tile> mPath;
	private int pos = 0;
	
	public Path() {
		mPath = new ArrayList<Tile>();
	}
	
	public int getIndex(){
		
		return pos;
	}
	
	public Tile getNext() {
		
		return mPath.get(pos++ % mPath.size());
	}
	
	
	public void setPath(ArrayList<Tile> path) {
		mPath = path;
	}
}
