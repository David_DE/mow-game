package de.thm.mow.game.td.model.map;

import de.thm.mow.game.td.model.GameObject;

public abstract class Tile extends GameObject {
	public enum TileType{
		ROAD_START(0), ROAD_NORMAL(1), ROAD_END(2), CONSTRUCTIONGROUND_EMPTY(3), CONSTRUCTIONGROUND_TOWER(4), WASTELAND(5);
		
		private final int type;
		
		private TileType(int type) {
			this.type = type;
		}
		
		public int getType() {
			return this.type;
		}
		
	}
	
	
	public static final int TILE_WIDTH = 64; //Todo Auslagern nach Tiles
	public TileType type;
	
	public Tile(int x, int y, TileType type) {
		super(x, y);
		this.x = x;
		this.y = y;
		this.type = type;
	}
	
	public abstract void clickedAt();
}
