package de.thm.mow.game.td.model.map;

public class ConstructionGround extends Tile {
	/**
	 * If -1, no tower has been build on the ConstructionGround
	 * If >= 0, a tower with the id buildingId is on this tile
	 */
	public int buildingId = -1;
	public int towerId = -1;

	public ConstructionGround(int x, int y, TileType type) {
		super(x, y, type);
	}

	@Override
	public void clickedAt() {
		// show building menu; build; link tile with building?
	}

}
