package de.thm.mow.game.td.model;

import android.util.Log;

public class UiElement {
	
	public enum UiType{
		UI_TOWER_BUILD(1), UI_RESSOURCE(2), UI_TOWER_UPGRADE(3), UI_TOWER_INFO(4), UI_GAME_SP_LOST(5), UI_GAME_PAUSED(6), 
		UI_RESSOURCE_ENEMY(7), UI_GAME_MP_WON(8), UI_GAME_MP_LOST(9), UI_GAME_SP_WON(10), UI_GENERAL(11), UI_TOWER_SELL(12), 
		UI_MP_MENU_SEND_UNITS(13);
		
		private final int type;
		
		private UiType(int type) {
			this.type = type;
		}
		
		public int getType() {
			return this.type;
		}
		
	}
	
	public float x;
	public float y;
	public int height;
	public int width;
	public UiType type;
	public boolean mIsShowing = false;
	
	public UiElement(int x, int y, int width, int height, UiType type) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.type = type;
	}
	
	public void reset() {
		mIsShowing = false;
		x = -1;
		y = -1;
	}

	public boolean clickedAtPoint(float x, float y) {
		return false;
	}

	public boolean clickedAtArea(float x, float y) {
		if ((x > this.x) && (x < (this.x + this.width))) {
			if ((y > this.y) && (y < (this.y + this.height))) {
				Log.d("clickfix", "clickedAtArea: true");
				return true;
			}
		}
		return false;
	}
}
