package de.thm.mow.game.td.model;

/**
 * GameObject describes a object, which is unable to move e.g. tower
 */
public class GameObject {
	public float x;
	public float y;
	
	public GameObject(float x, float y) {
		this.x = x;
		this.y = y;
	}

}
