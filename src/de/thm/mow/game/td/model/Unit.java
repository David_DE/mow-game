package de.thm.mow.game.td.model;

import java.util.ArrayList;

import de.thm.mow.game.td.model.map.Tile;
import de.thm.mow.game.td.model.map.Tile.TileType;

public class Unit extends MovableGameObject  {
	
	private Tile target;
	private Path mPath;
	public UnitType type;
	private int tileQuarter;

	public int hp;
	public int hp_current;
	public int bounty;
	public int strength;
	private boolean hasFinided;
	private boolean isDead;
	
	public Unit(ArrayList<Tile> path, UnitType type) {
		super(0,0);
		mPath = new Path();
		tileQuarter = Tile.TILE_WIDTH / 4;
		mPath.setPath(path);
		hasFinided = false;
		isDead = false;
		
		if (type == UnitType.SOLDIER){         
			this.type = UnitType.SOLDIER; 
			xSpeed = Stats.SPEED_UNIT_SOLDIER;
			ySpeed = Stats.SPEED_UNIT_SOLDIER;
			hp = hp_current = Stats.HEALTH_UNIT_SOLDIER;
			bounty = Stats.BOUNTY_UNIT_SOLDIER;
			strength = Stats.STRENGTH_UNIT_SOLDIER;
		}else if (type == UnitType.TANK){
			this.type = UnitType.TANK;
			xSpeed = Stats.SPEED_UNIT_TANK;
			ySpeed = Stats.SPEED_UNIT_TANK;
			hp = hp_current = Stats.HEALTH_UNIT_TANK;
			bounty = Stats.BOUNTY_UNIT_TANK;
			strength = Stats.STRENGTH_UNIT_TANK;
		}else if (type == UnitType.BOMB){
			this.type = UnitType.BOMB;
			xSpeed = Stats.SPEED_UNIT_BOMB;
			ySpeed = Stats.SPEED_UNIT_BOMB;
			hp = hp_current = Stats.HEALTH_UNIT_BOMB;
			bounty = Stats.BOUNTY_UNIT_BOMB;
			strength = Stats.STRENGTH_UNIT_BOMB;
		}
		target = mPath.getNext();
		
		y = target.y + tileQuarter;	
		x = target.x + tileQuarter;	
	}		
	
	public void update(float deltatime) {
		if (isBetween((target.x) + tileQuarter, (target.x) + (tileQuarter*2), x) 
				&& isBetween((target.y) + tileQuarter, (target.y) + (tileQuarter*2), y)) { //stop
			
			if (target.type==TileType.ROAD_END) {//
				hasFinided = true;
			}

			target = mPath.getNext();			
					
		}
		else if (isBetween((target.x) + tileQuarter, (target.x) + (tileQuarter*2), x) 
				&& y < (target.y + tileQuarter)) { //go down
			y += ySpeed * deltatime;
		} else if (isBetween((target.x) + tileQuarter, (target.x) + (tileQuarter*2), x) 
				&&  y > (target.y) + tileQuarter) { //go up
			y -= ySpeed * deltatime;
		} else if (isBetween((target.y) + tileQuarter, (target.y) + (tileQuarter*2), y) 
				&& x < (target.x) + tileQuarter) { //go right
			x += xSpeed * deltatime;
		} else if (isBetween((target.y) + tileQuarter, (target.y) + (tileQuarter*2), y) 
				&& x > (target.x) + tileQuarter) { //go left
			x -= xSpeed * deltatime;
		}		
	}
	
	public boolean isBetween(float a, float b, float c) {
	    return b >= a ? c >= a && c <= b : c >= b && c <= a;
	}
	public boolean hasFinished(){
		return hasFinided;
	}
	public boolean isDead(){
		return isDead;
	}
	public boolean removeHealth(int h){
		hp_current -= h;
		
		isDead = (hp_current <= 0);
		return !(hp_current <= 0);
	}	
	
	public enum UnitType{
		TANK(1), SOLDIER(2), BOMB(3);
		
		private final int type;
		
		private UnitType(int type) {
			this.type = type;
		}
		
		public int getType() {
			return this.type;
		}	
		
		public static UnitType getInstance(int type) {
			switch (type) {
			case 1:
				return UnitType.TANK;
			case 2:
				return UnitType.SOLDIER;
			case 3:
				return UnitType.BOMB;

			default:
				return null;
			}
		}
		
	}
}
