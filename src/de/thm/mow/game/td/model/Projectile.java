package de.thm.mow.game.td.model;

import android.util.Log;
import de.thm.mow.game.td.ProjectileManager;
import de.thm.mow.game.td.model.map.Tile;
import de.thm.mow.game.td.model.Stats;

/**
 * Projectile extends a MoveableGameObject and describes a projectile flying from a tower towards a unit
 */
public class Projectile extends MovableGameObject {

	public enum ProjectileType {
		AMMO_MG(1), AMMO_FLAME(2), AMMO_LIGHTNING(3), AMMO_ICE(4);

		private final int type;

		private ProjectileType(int type) {
			this.type = type;
		}

		public int getType() {
			return this.type;
		}
	}
	/**
	 * the projectiles target, the unit
	 */
	public Unit mTarget;
	/**
	 * the projectiles start, the tower
	 */
	public Tower mTower;
	public float speed = 10;
	public float angle;
	public int damage;
	public ProjectileManager mProjectileManager;

	public ProjectileType type;

	public Projectile(Unit u, Tower t, float angle, ProjectileManager pm) {
		super(t.x, t.y);
		mTarget = u;
		mTower = t;
		this.angle = angle;
		mProjectileManager = pm;
		x = mTower.x + Tile.TILE_WIDTH / 2;
		y = mTower.y + Tile.TILE_WIDTH / 2;
		
		switch (mTower.type) {
			case TOWER_MG_1:
				type = ProjectileType.AMMO_MG;
				damage = Stats.DAMAGE_TOWER_MG_I;
				break;
			case TOWER_MG_2:
				type = ProjectileType.AMMO_MG;
				damage = Stats.DAMAGE_TOWER_MG_II;
				break;
			case TOWER_MG_3:
				type = ProjectileType.AMMO_MG;
				damage = Stats.DAMAGE_TOWER_MG_III;
				break;
			case TOWER_FLAME_1:
				type = ProjectileType.AMMO_FLAME;
				damage = Stats.DAMAGE_TOWER_FLAME_I;
				break;
			case TOWER_FLAME_2:
				type = ProjectileType.AMMO_FLAME;
				damage = Stats.DAMAGE_TOWER_FLAME_II;
				break;
			case TOWER_FLAME_3:
				type = ProjectileType.AMMO_FLAME;
				damage = Stats.DAMAGE_TOWER_FLAME_III;
				break;
			case TOWER_ICE_1:
				type = ProjectileType.AMMO_ICE;
				damage = Stats.DAMAGE_TOWER_ICE_I;
				break;
			case TOWER_ICE_2:
				type = ProjectileType.AMMO_ICE;
				damage = Stats.DAMAGE_TOWER_ICE_II;
				break;
			case TOWER_ICE_3:
				type = ProjectileType.AMMO_ICE;
				damage = Stats.DAMAGE_TOWER_ICE_III;
				break;
			case TOWER_LIGHTNING_1:
				type = ProjectileType.AMMO_LIGHTNING;
				damage = Stats.DAMAGE_TOWER_LIGHTNING_I;
				break;
			case TOWER_LIGHTNING_2:
				type = ProjectileType.AMMO_LIGHTNING;
				damage = Stats.DAMAGE_TOWER_LIGHTNING_II;
				break;
			case TOWER_LIGHTNING_3:
				type = ProjectileType.AMMO_LIGHTNING;
				damage = Stats.DAMAGE_TOWER_LIGHTNING_III;
				break;
		}
		
	}

	/**
	 * checks if the distance between a projectile and a unit is smaller than a units size
	 * if yes, a unit is hit, the projectile gets deleted and health is removed from the unit
	 */
	public void update(float deltaTime) {
		calcSpeed();	
		x = (x+(xSpeed*speed));
		y = (y+(ySpeed*speed));
		
		// if a projectile hits a unit, the projectile is deleted and the unit loses health
		if(Math.sqrt((Math.pow(x-(mTarget.x+16),2))+(Math.pow(y-(mTarget.y+16),2))) <= 32){
			mTarget.removeHealth(damage);
			mProjectileManager.projectilesToRemove(this);
		}
		
		// if projectile doesn't hit anything, it is deleted after 1000 pixel distance from its tower
		if(Math.sqrt((Math.pow(x-mTower.x,2))+(Math.pow(y-mTower.y,2))) >= 1000){				
			Log.d("Projectile", "nothing hit! projectile deleted");
			mProjectileManager.projectilesToRemove(this);		
		}
	}

	/**
	 * calculates the current x and y coordinates for a projectile flying towards a unit
	 */
	private void calcSpeed() {	
	    ySpeed = (float) (Math.sin(angle * (Math.PI / 180)) * 2);
	    xSpeed = (float) (Math.cos(angle * (Math.PI / 180)) * 2);     
	}
}
