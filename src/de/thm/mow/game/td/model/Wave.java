package de.thm.mow.game.td.model;

import java.util.ArrayList;

import de.thm.mow.game.td.model.Unit.UnitType;

public class Wave {
	private ArrayList<UnitType> wave;
	private Boolean hasAnextWave = true;
	
	public Wave(int number){
		wave = new ArrayList<UnitType>();
		switch (number) {
		case 0:
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			hasAnextWave = true;
			break;
		case 1: 
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			hasAnextWave = true;
			break;
		case 2: 
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			hasAnextWave = true;
			break;
		case 3: 
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);			
			hasAnextWave = true;
			break;
		case 4: 
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.BOMB);
			hasAnextWave = true;
			break;
		case 5: 
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.BOMB);			
			hasAnextWave = true;
			break;
		case 6: 
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.BOMB);			
			hasAnextWave = true;
			break;
		case 7: 
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			wave.add(UnitType.TANK);
			hasAnextWave = true;
			break;
		case 8: 
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.SOLDIER);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.SOLDIER);	
			hasAnextWave = true;
			break;
		case 9: 
			wave.add(UnitType.TANK);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.TANK);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.TANK);
			wave.add(UnitType.BOMB);
			wave.add(UnitType.TANK);
			wave.add(UnitType.BOMB);
			hasAnextWave = true;
			break;
		default:					
			hasAnextWave = false;
			break;
		}
		
	}
	public ArrayList<UnitType> getWave(){
		return wave;
	}
	public Boolean hasAnextWave(){
		return hasAnextWave;
	}
}
