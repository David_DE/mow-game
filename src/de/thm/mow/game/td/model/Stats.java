package de.thm.mow.game.td.model;

public class Stats {
	
	//TOWER
	//COST
	public static int COST_TOWER_MG_I = 100;
	public static int COST_TOWER_MG_II = 75;
	public static int COST_TOWER_MG_III = 150;

	public static int COST_TOWER_FLAME_I = 120;
	public static int COST_TOWER_FLAME_II = 160;
	public static int COST_TOWER_FLAME_III = 200;
	
	public static int COST_TOWER_ICE_I = 150;
	public static int COST_TOWER_ICE_II = 200;
	public static int COST_TOWER_ICE_III = 260;
	
	public static int COST_TOWER_LIGHTNING_I = 250;
	public static int COST_TOWER_LIGHTNING_II = 220;
	public static int COST_TOWER_LIGHTNING_III = 300;
	
	//DAMAGE
	public static int DAMAGE_TOWER_MG_I = 20;
	public static int DAMAGE_TOWER_MG_II = 40;
	public static int DAMAGE_TOWER_MG_III = 60;

	public static int DAMAGE_TOWER_FLAME_I = 10;
	public static int DAMAGE_TOWER_FLAME_II = 20;
	public static int DAMAGE_TOWER_FLAME_III = 40;
	
	public static int DAMAGE_TOWER_ICE_I = 10;
	public static int DAMAGE_TOWER_ICE_II = 15;
	public static int DAMAGE_TOWER_ICE_III = 30;
	
	public static int DAMAGE_TOWER_LIGHTNING_I = 100;
	public static int DAMAGE_TOWER_LIGHTNING_II = 150;
	public static int DAMAGE_TOWER_LIGHTNING_III = 225;
	
	//RELOAD
	public static int RELOAD_TOWER_MG_I = 1000;
	public static int RELOAD_TOWER_MG_II = 800;
	public static int RELOAD_TOWER_MG_III = 650;
	
	public static int RELOAD_TOWER_FLAME_I = 250;
	public static int RELOAD_TOWER_FLAME_II = 250;
	public static int RELOAD_TOWER_FLAME_III = 250;
	
	public static int RELOAD_TOWER_ICE_I = 1500;
	public static int RELOAD_TOWER_ICE_II = 1500;
	public static int RELOAD_TOWER_ICE_III = 1500;
	
	public static int RELOAD_TOWER_LIGHTNING_I = 3000;
	public static int RELOAD_TOWER_LIGHTNING_II = 2800;
	public static int RELOAD_TOWER_LIGHTNING_III = 2500;
	
	//RANGE
	public static int RANGE_TOWER_MG_I = 192;
	public static int RANGE_TOWER_MG_II = 224;
	public static int RANGE_TOWER_MG_III = 256;
	
	public static int RANGE_TOWER_FLAME_I = 118;
	public static int RANGE_TOWER_FLAME_II = 118;
	public static int RANGE_TOWER_FLAME_III = 118;
	
	public static int RANGE_TOWER_ICE_I = 192;
	public static int RANGE_TOWER_ICE_II = 224;
	public static int RANGE_TOWER_ICE_III = 256;
	
	public static int RANGE_TOWER_LIGHTNING_I = 250;
	public static int RANGE_TOWER_LIGHTNING_II = 275;
	public static int RANGE_TOWER_LIGHTNING_III = 300;
	
	//Units
	
	public static int HEALTH_UNIT_SOLDIER = 100;
	public static int HEALTH_UNIT_TANK = 500;
	public static int HEALTH_UNIT_BOMB = 1500;

	public static int BOUNTY_UNIT_SOLDIER = 25;
	public static int BOUNTY_UNIT_TANK = 100;
	public static int BOUNTY_UNIT_BOMB = 250;

	public static float SPEED_UNIT_SOLDIER = 2.5f;
	public static float SPEED_UNIT_TANK = 2f;
	public static float SPEED_UNIT_BOMB = 1f;

	public static int STRENGTH_UNIT_SOLDIER = 1;
	public static int STRENGTH_UNIT_TANK = 2;
	public static int STRENGTH_UNIT_BOMB = 5;
	
	public static final int COST_UNIT_SOLDIER = 25;
	public static final int COST_UNIT_TANK = 150;
	public static final int COST_UNIT_BOMB = 500;
	
	
	
	
	
	
	
}
