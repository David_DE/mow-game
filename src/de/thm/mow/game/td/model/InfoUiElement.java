package de.thm.mow.game.td.model;

public class InfoUiElement extends UiElement {
	public enum InfoType{
		BUILD_TOWER_MG_I(1), BUILD_TOWER_LIGHTNING_I(2), BUILD_TOWER_ICE_I(3), BUILD_TOWER_FLAME_I(4),
		UPGRADE_MG(5), UPGRADE_LIGHTNING(6), UPGRADE_ICE(7), UPGRADE_FLAME(8), SELL_TOWER(9),
		UPGRADE_MG_2(10), UPGRADE_LIGHTNING_2(11), UPGRADE_ICE_2(12), UPGRADE_FLAME_2(13), GENERAL(14);
		
		private final int type;
		
		private InfoType(int type) {
			this.type = type;
		}
		
		public int getType() {
			return this.type;
		}
		
	}
	
	public InfoType info;

	public InfoUiElement(int x, int y, int width, int height, UiType type) {
		super(x, y, width, height, type);
	}

}
