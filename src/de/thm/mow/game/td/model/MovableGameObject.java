package de.thm.mow.game.td.model;

/**
 * MovableGameObject extends GameObject and describes a GameObject, which is able to move e.g. projectile or unit
 */
public class MovableGameObject extends GameObject {
	
	public float xSpeed;
	public float ySpeed;

	public MovableGameObject(float x, float y) {
		super(x, y);
		xSpeed = 0;
		ySpeed = 0;
	}

}
