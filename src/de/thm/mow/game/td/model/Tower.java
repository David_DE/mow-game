package de.thm.mow.game.td.model;

import java.util.ArrayList;

import de.thm.mow.game.td.ProjectileManager;
import de.thm.mow.game.td.model.map.Tile;

public class Tower extends GameObject {
	public TowerType type;
	public int range;
	public float angle = 0;
	public ArrayList<Unit> units;
	public ProjectileManager mProjectilManager;

	private int towerId;

	float reload = 0;
	int RELOAD_SPEED = 1000;
	boolean readyToFire = true;

	public enum TowerType {
		TOWER_MG_1(1), TOWER_MG_2(2), TOWER_MG_3(3), 
		TOWER_FLAME_1(4), TOWER_FLAME_2(5), TOWER_FLAME_3(6), 
		TOWER_ICE_1(7), TOWER_ICE_2(8), TOWER_ICE_3(9), 
		TOWER_LIGHTNING_1(10), TOWER_LIGHTNING_2(11), TOWER_LIGHTNING_3(12);

		private final int type;

		private TowerType(int type) {
			this.type = type;
		}

		public int getType() {
			return this.type;
		}

		public static TowerType getInstance(int i) {
			switch (i) {
			case 1:
				return TOWER_MG_1;
			case 2:
				return TOWER_MG_2;
			case 3:
				return TOWER_MG_3;
			case 4:
				return TOWER_FLAME_1;
			case 5:
				return TOWER_FLAME_2;
			case 6:
				return TOWER_FLAME_3;
			case 7:
				return TOWER_ICE_1;
			case 8:
				return TOWER_ICE_2;
			case 9:
				return TOWER_ICE_3;
			case 10:
				return TOWER_LIGHTNING_1;
			case 11:
				return TOWER_LIGHTNING_2;
			case 12:
				return TOWER_LIGHTNING_3;

			default:
				break;
			}
			return null;
		}

	}

	public Tower(float x, float y, TowerType type, int id) {
		super(x, y);
		this.type = type;
		towerId = id;
		setStats();
	}

	public void setPm(ProjectileManager pm) {
		this.mProjectilManager = pm;
	}

	public void setUnitList(ArrayList<Unit> units) {
		this.units = units;
	}

	public void update(float deltaTime) {
		if (!readyToFire) {
			if ((reload += (1000 / 60) * deltaTime) > RELOAD_SPEED) {
				reload -= RELOAD_SPEED;
				readyToFire = true;
			}
		}

		for (Unit u : units) {
			if (calcDistanceTo(u) < range) {
				angle = calculateAngleTo(u.x + 16, u.y + 16);

				// dirty fix: tower not rotating in opposite direction anymore
				// when unit is directly above it
				if (angle == 90) {
					angle = -90;
				} else if (angle == -90) {
					angle = 90;
				}

				if (readyToFire) {
					shootAt(u);
					readyToFire = false;
				}
				break;
			}
		}
	}

	public int getTowerId() {
		return towerId;
	}

	/**
	 * a new projectile is generated and flies towards the Unit u
	 * 
	 * @param u Unit
	 */

	private void shootAt(Unit u) {
		Projectile p = new Projectile(u, this, angle, mProjectilManager);
		mProjectilManager.addProjectile(p);
	}

	/**
	 * the distance between a tower and a unit is calculated
	 * 
	 * @param u Unit
	 */
	private int calcDistanceTo(Unit u) {
		int wh = 32;

		return (int) Math.sqrt(Math.pow(this.x + Tile.TILE_WIDTH / 2 - (u.x + wh), 2) + Math.pow(this.y + Tile.TILE_WIDTH / 2 - (u.y + wh), 2));
	}

	/**
	 * the angle a tower has to rotate to face a unit is calculated
	 * 
	 * @param xUnit x-coordinate of the unit
	 * @param yUnit y-coordinate of the unit
	 */
	public float calculateAngleTo(float xUnit, float yUnit) {

		float angle_rad = (float) Math.atan((this.y + (Tile.TILE_WIDTH / 2) - yUnit) / (this.x + (Tile.TILE_WIDTH / 2) - xUnit));
		float angle_deg = (float) Math.toDegrees(angle_rad);

		if (xUnit < (this.x + (Tile.TILE_WIDTH / 2))) {
			angle_deg = (angle_deg + 180) % 360;
		}

		return (angle_deg);
	}

	public void upgrade() {

		switch (type) {
		case TOWER_FLAME_1:
			type = TowerType.TOWER_FLAME_2;
			break;
		case TOWER_FLAME_2:
			type = TowerType.TOWER_FLAME_3;
			break;
//		case TOWER_FLAME_3:
//			type = TowerType.TOWER_FLAME_1;
//			break;
		case TOWER_ICE_1:
			type = TowerType.TOWER_ICE_2;
			break;
		case TOWER_ICE_2:
			type = TowerType.TOWER_ICE_3;
			break;
//		case TOWER_ICE_3:
//			type = TowerType.TOWER_ICE_1;
//			break;
		case TOWER_LIGHTNING_1:
			type = TowerType.TOWER_LIGHTNING_2;
			break;
		case TOWER_LIGHTNING_2:
			type = TowerType.TOWER_LIGHTNING_3;
			break;
//		case TOWER_LIGHTNING_3:
//			type = TowerType.TOWER_LIGHTNING_1;
//			break;
		case TOWER_MG_1:
			type = TowerType.TOWER_MG_2;
			break;
		case TOWER_MG_2:
			type = TowerType.TOWER_MG_3;
			break;
//		case TOWER_MG_3:
//			type = TowerType.TOWER_MG_1;
//			break;

		default:
			break;
		}

		setStats();
	}

	/**
	 * sets range and reload-speed for each tower
	 */
	private void setStats() {
		switch (type) {
		case TOWER_MG_1:
			range = Stats.RANGE_TOWER_MG_I;
			RELOAD_SPEED = Stats.RELOAD_TOWER_MG_I;
			break;
		case TOWER_MG_2:
			range = Stats.RANGE_TOWER_MG_II;
			RELOAD_SPEED = Stats.RELOAD_TOWER_MG_II;
			break;
		case TOWER_MG_3:
			range = Stats.RANGE_TOWER_MG_III;
			RELOAD_SPEED = Stats.RELOAD_TOWER_MG_III;
			break;
		case TOWER_FLAME_1:
			range = Stats.RANGE_TOWER_FLAME_I;
			RELOAD_SPEED = Stats.RELOAD_TOWER_FLAME_I;
			break;
		case TOWER_FLAME_2:
			range = Stats.RANGE_TOWER_FLAME_II;
			RELOAD_SPEED = Stats.RELOAD_TOWER_FLAME_II;
			break;
		case TOWER_FLAME_3:
			range = Stats.RANGE_TOWER_FLAME_III;
			RELOAD_SPEED = Stats.RELOAD_TOWER_FLAME_III;
			break;
		case TOWER_ICE_1:
			range = Stats.RANGE_TOWER_ICE_I;
			RELOAD_SPEED = Stats.RELOAD_TOWER_ICE_I;
			break;
		case TOWER_ICE_2:
			range = Stats.RANGE_TOWER_ICE_II;
			RELOAD_SPEED = Stats.RELOAD_TOWER_ICE_II;
			break;
		case TOWER_ICE_3:
			range = Stats.RANGE_TOWER_ICE_III;
			RELOAD_SPEED = Stats.RELOAD_TOWER_ICE_III;
			break;
		case TOWER_LIGHTNING_1:
			range = Stats.RANGE_TOWER_LIGHTNING_I;
			RELOAD_SPEED = Stats.RELOAD_TOWER_LIGHTNING_I;
			break;
		case TOWER_LIGHTNING_2:
			range = Stats.RANGE_TOWER_LIGHTNING_II;
			RELOAD_SPEED = Stats.RELOAD_TOWER_LIGHTNING_II;
			break;
		case TOWER_LIGHTNING_3:
			range = Stats.RANGE_TOWER_LIGHTNING_III;
			RELOAD_SPEED = Stats.RELOAD_TOWER_LIGHTNING_III;
			break;
		default:
			break;
		}
	}
}
