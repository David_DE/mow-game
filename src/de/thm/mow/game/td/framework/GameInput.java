package de.thm.mow.game.td.framework;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import de.thm.mow.game.td.GameView;
import de.thm.mow.game.td.framework.TouchEvent.TouchEventType;

/**
 * GameInput handels touch events on the {@link GameView} and 
 * collects them in an ArrayListe as {@link TouchEvent}.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class GameInput {
	/**
	 * If the distance between touch down and touch up is smaller than
	 * SCROLL_THRESHOLD it is considered a tap and not a move event.
	 */
	private static final float SCROLL_THRESHOLD = 1.0f;
	
	/**
	 * After LONG_TAP_TRESHOLD amounts of ms the tap is now 
	 * a long tap
	 */
	private static long LONG_TAP_THRESHOLD;
	protected static final String TAG = "GameInput";
	private GameView mView;
	private ArrayList<TouchEvent> mTouchEvents;

	private float mScaleX;
	private float mScaleY;
	
	private float mLastX = 0;
	private float mLastY = 0;
	private boolean isAClick = false;
	protected boolean wasMoved = false;
	private long lastTimeDown = 0;

	/**
	 * 
	 * @param view - The {@link GameView} where the touch events occure
	 * @param scaleX - Scale value to support different screen sizes 
	 * @param scaleY - Scale value to support different screen sizes 
	 */
	public GameInput(GameView view, float scaleX, float scaleY) {
		mView = view;
		mTouchEvents = new ArrayList<TouchEvent>();
		
		mScaleX = scaleX;
		mScaleY = scaleY;
		
		LONG_TAP_THRESHOLD = ViewConfiguration.getLongPressTimeout();
		
		mView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				synchronized (this) {
					int action = event.getAction() & MotionEvent.ACTION_MASK;
					final float x = event.getX() / mScaleX;
					final float y = event.getY() / mScaleY;
					final long eventtime = event.getEventTime();

					switch (action) {
					case MotionEvent.ACTION_DOWN:
						isAClick = true;
						wasMoved = false;
						lastTimeDown = eventtime;
						mLastX = x;
						mLastY = y;
						break;
					case MotionEvent.ACTION_UP:
						if (isAClick && !wasMoved) {
							if ((eventtime) - lastTimeDown < LONG_TAP_THRESHOLD) {
								TouchEvent tempEvent = new TouchEvent(x, y);
								tempEvent.type = TouchEventType.TAP;
								mTouchEvents.add(tempEvent);
							}
						}
						break;
					case MotionEvent.ACTION_MOVE:
						float dx = mLastX - x;
						float dy = mLastY - y;
						if (isAClick && Math.abs(dy) > SCROLL_THRESHOLD && Math.abs(dx) > SCROLL_THRESHOLD) {
							TouchEvent tempEvent = new TouchEvent(dx, dy);
							tempEvent.type = TouchEventType.SCROLL;
							mTouchEvents.add(tempEvent);
							wasMoved = true;
						}
						mLastX = x;
						mLastY = y;
						break;
					default:
						Log.e(TAG, "Should never happen!! in GameInput()");
						break;
					}

					return false;
				}
			}
		});
		
		mView.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				Log.d("longtouch", "Long! @ " + mLastX + " / " + mLastY);
				
				TouchEvent tempEvent = new TouchEvent(mLastX, mLastY);
				tempEvent.type = TouchEventType.LONG_TAP;
				mTouchEvents.add(tempEvent);
				
				return true;
			}
		});
		
	}

	/**
	 * getTouchEvents() gives you an {@link ArrayList} of every occured {@link TouchEvent}
	 * since the last time this function was called.
	 * 
	 * @return ArrayList<TouchEvents> of {@link TouchEvent}
	 */
	public ArrayList<TouchEvent> getTouchEvents() {
		ArrayList<TouchEvent> temp = new ArrayList<TouchEvent>(mTouchEvents);
		mTouchEvents.clear();
		return temp;
	}

}
