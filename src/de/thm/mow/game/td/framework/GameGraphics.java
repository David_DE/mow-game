package de.thm.mow.game.td.framework;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import de.thm.mow.game.td.GameView;
import de.thm.mow.game.td.model.map.Tile;

/**
 * GameGraphics soll Funktionen zum zeichnen auf die Canvas/den framebuffer beinhalten.
 * z.b. rect/text/bmp zeichnen
 *
 */
public class GameGraphics {
	/**
	 * Alle Zeichenfunktionen werden ueber die Canvas auf dem frameBuffer ausgefuehrt. 
	 * Gleichzeitig wird dieser in der {@link GameView} auf den Bildschirm gerendert.
	 */
	private Bitmap frameBuffer;
	private Canvas canvas;
	private Paint mPaint;

	public GameGraphics(Bitmap frameBuffer) {
		this.frameBuffer = frameBuffer;
		canvas = new Canvas(frameBuffer);
		mPaint = new Paint();
		
		// Enables Anti-Aliasing when drawing
		mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		mPaint.setAntiAlias(true);
		mPaint.setFilterBitmap(true);
		//mPaint.setDither(true); //groessere farbtiefe bei "schwaecheren/aelteren" geraeten 
	}
	
	public void drawCircle(float cx, float cy, float radius, int color) {
		mPaint.setColor(color);
		canvas.drawCircle(cx, cy, radius, mPaint);
	}
	
	/**
	 * Diese Funktion zeichnet eine Bitmap auf das Canvas
	 * @param bitmap Die Bitmap die gezeichnet werden soll
	 * @param left Die x Position der linken Kante
	 * @param top Die x Position der oberen Kante
	 * @param p Paint
	 */
	public void drawBitmap(Bitmap bitmap, float left, float top, Paint p) {
		canvas.drawBitmap(bitmap, left, top, p);
	}
	public void drawBitmap(Bitmap bitmap, float left, float top) {
		canvas.drawBitmap(bitmap, left, top, mPaint);
	}
	
	public void drawBitmapScaled(Bitmap bitmap, Rect src, Rect dst, Paint p) {
		canvas.drawBitmap(bitmap, src, dst, p);
	}
	
	public void drawRotatedBitmap(Bitmap bitmap, float left, float top, float angle){
		Matrix rotator = new Matrix();
		rotator.postTranslate(- Tile.TILE_WIDTH/2, - Tile.TILE_WIDTH/2); // move pivot point to 0/0
		rotator.postRotate(angle+90); // rotate around pivot
		rotator.postTranslate(left + Tile.TILE_WIDTH/2, top + Tile.TILE_WIDTH/2); //move top koords + Tile width/2
		canvas.drawBitmap(bitmap, rotator, mPaint);
	}
	
	public void drawRect(float left, float top, float right, float bottom, int color) {
		mPaint.setColor(color);
		canvas.drawRect(left, top, right, bottom, mPaint);
	}
	
	public void drawTextStandard(String text, float x, float y, int color) {
		mPaint.setColor(color);
		mPaint.setTextSize(30);
		canvas.drawText(text, x, y, mPaint);
	}
	
	public void drawText(String text, float x, float y, int color, int size) {
		mPaint.setColor(color);
		mPaint.setTextSize(size);
		canvas.drawText(text, x, y, mPaint);
	}
	
	public int getWidth() {
		return frameBuffer.getWidth();
	}

	public int getHeight() {
		return frameBuffer.getHeight();
	}

}
