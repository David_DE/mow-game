package de.thm.mow.game.td.framework;

import de.thm.mow.game.td.AndroidGame;

/**
 * Screen Klasse stellt einen einzelnen "Bildschirm" dar (abstract). Zwischen Screens soll gewechselt werden koennen
 * z.B. LoadingScreen -> MainMenuScreen -> GameScreen
 */
public abstract class Screen {
	public int offsetX = 0;
	public int offsetY = 0;
	public int deltaOffsetX = 0;
	public int deltaOffsetY = 0;
	protected final AndroidGame game;
	
	public Screen(AndroidGame game) {
		this.game = game;
	}
	
	public abstract void update(float deltaTime);
	public abstract void draw(float deltaTime);
	public abstract void pause();
	public abstract void resume();
	public abstract void dispose();
	
}