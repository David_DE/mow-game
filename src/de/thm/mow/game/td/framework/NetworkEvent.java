package de.thm.mow.game.td.framework;

import de.thm.mow.game.td.mp.MultiplayerEventData;

public class NetworkEvent {
	private int playerCnt, ownId, msgType;
	private String activityName;	
	private String[] playerNames;
	private int mpType, adressId, closedById;	
	private MultiplayerEventData multiplayerEventdata;
	
	public NetworkEvent(int msgType, int playerCnt,int ownId, String player, String activity) {
		this.msgType = msgType;
		this.playerCnt = playerCnt;
		this.ownId = ownId;
		
		if (player == null) {
			player = "";
		} 
		playerNames = player.split(";");
		activityName = activity;		
	}	
	
	public NetworkEvent(int msgType, int adressId, int mpType, MultiplayerEventData data){
		this.msgType = msgType;
		this.adressId = adressId;
		this.mpType = mpType;
		multiplayerEventdata = data;		
	}
	
	public NetworkEvent(int msgType, int closedById){
		this.msgType = msgType;
		this.closedById = closedById;		
	}
	
	public int getMsgType() {
		return msgType;
	}

	public int getMpType(){
		return mpType;
	}
	
	public int getAdressId(){
		return adressId;
	}
	
	public MultiplayerEventData getMultiPlayerEventData(){
		return multiplayerEventdata;
	}

	public int getPlayerCnt() {
		return playerCnt;
	}
	
	public int getOwnId(){
		return ownId;
	}
	
	public String getActivityName(){
		return activityName;
	}
	
	public String[] getPlayerNames(){
		return playerNames;
	}
	
	public int getClosedById(){
		return closedById;
	}
	
	

	
}
