package de.thm.mow.game.td.framework;

public class TouchEvent {
	public enum TouchEventType{
		TAP(1), SCROLL(2), LONG_TAP(3), PINCH_IN(4), PINCH_OUT(5);
		
		private final int type;
		
		private TouchEventType(int type) {
			this.type = type;
		}
		
		public int getType() {
			return this.type;
		}
		
	}
	
	public float x;
	public float y;
	public TouchEventType type;
	
	public TouchEvent(int x, int y, TouchEventType type) {
		this.x = x;
		this.y = y;
		this.type = type;		
	}
	public TouchEvent(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
}
