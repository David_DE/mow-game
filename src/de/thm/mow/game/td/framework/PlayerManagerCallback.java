package de.thm.mow.game.td.framework;

import de.thm.mow.game.td.model.Unit.UnitType;

public interface PlayerManagerCallback {
	public void sendUnitsToAllButLocal(UnitType type);
}
