package de.thm.mow.game.td.framework;

import java.util.ArrayList;

import de.thm.mow.game.td.AndroidGame;
import de.thm.mow.game.td.mp.ClassChooserDialog;
import de.thm.mow.game.td.mp.IncomingHandler;
import de.thm.mow.game.td.mp.MOWSchnittstellenInterface;
import de.thm.mow.game.td.mp.ClassChooserDialog.ClassChooserDialogListener;
import de.thm.mow.game.td.mp.MultiplayerEventData;
import de.thm.mow.game.td.mp.MultiplayerEventData.MultiplayerEventDataType;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.app.FragmentManager;
import android.util.Log;

public class GameNetwork implements ClassChooserDialogListener {
//	private AndroidGame mGame; TODO: delete?
	private Context mContext;
	private FragmentManager mFragmentManager;
	
	public static final String TAG = "netzwerk";
	private PackageInfo mPackageInfo;
	private Messenger mService;
	private boolean mBound = false;
	
	private ArrayList<NetworkEvent> mNetworkEvents;

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "onServiceDisconnected: " + name.toString());
			mService = null;
			mBound = false;

		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "onServiceConnected: " + name.toString() + " / " + service.toString());
			mService = new Messenger(service);
			mBound = true;
			sendRegistrationMessage();
		}
	};

	private Messenger mMessenger;
	
	
	public GameNetwork(AndroidGame game, Context context, FragmentManager fragmentManager) {
//		mGame = game;
		mContext = context;
		mFragmentManager = fragmentManager;
		mNetworkEvents = new ArrayList<NetworkEvent>();
		
		mMessenger = new Messenger(new IncomingHandler(this));
	}
	
	public void showChooser() {
		new ClassChooserDialog().show(mFragmentManager, "TAG");
	}
	
	@Override
	public void setSelectedService(PackageInfo service) {
		Log.d(TAG, "setSelectedService" + service.toString());
		mPackageInfo = service;
		doBind();
	}

	private void doBind() {
		Log.d(TAG, "doBind");
		Intent intent = new Intent();

		intent.setClassName(mPackageInfo.packageName, mPackageInfo.services[0].name);
		Log.d(TAG, "setClassName: " + mPackageInfo.packageName + " / " + mPackageInfo.services[0].name);
		mContext.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

	}
	
	public void doUnbind() {
		Log.d(TAG, "doUnbind: " + mBound);
		if (mBound) {
			mContext.unbindService(mServiceConnection);
			mBound = false;
			Log.d(TAG, "ist unboundet");
		}
	}

	public void sendRegistrationMessage() {
		if (!mBound)
			return;

		Message msg = Message.obtain(null, MOWSchnittstellenInterface.MESSAGE_I_REGISTER, 0, 0);
		msg.replyTo = mMessenger;
		Log.d(TAG, "sendRegistrationMessage()");
		
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			Log.d(TAG, "Error in sendRegistrationMessage(): " + e.toString());
			e.printStackTrace();
		}
	}

	public void sendDataMessage(Parcelable p) {
		if (!mBound)
			return;


		final int type = p.describeContents();
		final Parcel parcel = Parcel.obtain();
		p.writeToParcel(parcel, 0);
		final byte[] data = parcel.marshall();
		final Bundle bundle = new Bundle(3);
		
		bundle.putByteArray(MOWSchnittstellenInterface.SEND_DATA, data);
		bundle.putString(MOWSchnittstellenInterface.SEND_TYPE, "" + type);
		bundle.putInt(MOWSchnittstellenInterface.SEND_ADDRESS, -1);
		
		Message msg = Message.obtain(null, MOWSchnittstellenInterface.MESSAGE_3_SEND);
		msg.setData(bundle);
		msg.replyTo = mMessenger;
		
		Log.d(TAG, "sendDataMessage()");
		parcel.recycle();
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			Log.d(TAG, "Error in sendDataMessage(): " + e.toString());
			e.printStackTrace();
		}
	}
	
	public void sendStartGameMessage(int ownId) {
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_GAME_START, ownId);
		int[] data = { ownId };
		event.setData(data);
		
		sendDataMessage(event);
	}

	public void sendEndGameMessage(int ownId, int lifesLeft) {
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_GAME_END, ownId);
		int[] data = { ownId, lifesLeft };
		event.setData(data);
		
		sendDataMessage(event);
	}
	
	public void sendLostLifeDataMessage(int ownId, int strength) {
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_PLAYER_LIFE, ownId);
		int[] data = { strength };
		event.setData(data);
		
		sendDataMessage(event);
	}
	
	public void sendSendUnitMessage(int type) {
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_PLAYER_SEND_UNIT, 0);
		int[] data = { type };
		event.setData(data);
		
		sendDataMessage(event);
	}

	public void sendTowerBuildDataMessage(int tileIndex, int towertype) { // 0 = x, 1 = y, 2 = towertype
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_PLAYER_TOWER_BUILD, 0);
		int[] data = { tileIndex, towertype };
		event.setData(data);	
		sendDataMessage(event);
	}
	
	public void sendTowerUpgradeDataMessage(int towerId){
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_PLAYER_TOWER_UPGRADE, 0);
		int[] data = { towerId };
		event.setData(data);	
		sendDataMessage(event);
	}
	
	public void sendTowerSellDataMessage(int id, int tileIndex) {
		MultiplayerEventData event = new MultiplayerEventData(MultiplayerEventDataType.EVENT_PLAYER_TOWER_SELL, 0);
		int[] data = { id, tileIndex };
		event.setData(data);	
		sendDataMessage(event);
		
	}

	public void sendCloseMessage() {
		if (!mBound)
			return;
		
		Message msg = Message.obtain(null, MOWSchnittstellenInterface.MESSAGE_5_CLOSE);
		msg.replyTo = mMessenger;
		Log.d(TAG, "sendCloseMessage()");
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			Log.d(TAG, "Error in sendCloseMessage(): " + e.toString());
			e.printStackTrace();
		}
	}
	
	/**
	 * Add the given event to an MultiplayerEvent ArrayList
	 * @param event
	 */
	public void addNetworkEvent(NetworkEvent event) {
		mNetworkEvents.add(event);
	}
	
	/**
	 * Removes the given event from the MultiplayerEvent ArrayList
	 * @param event
	 */
	public void removeNetworkEvent(NetworkEvent event){
		mNetworkEvents.remove(event);
	}
	
	/**
	 * Return the multiplayer events since last time;
	 * @return temp
	 */
	public ArrayList<NetworkEvent> getNetworkEvents() {
		ArrayList<NetworkEvent> temp = new ArrayList<NetworkEvent>(mNetworkEvents);
		mNetworkEvents.clear();
		return temp;
	}
	
	public boolean isBound() {
		return mBound;
	}
}
