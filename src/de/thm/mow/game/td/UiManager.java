package de.thm.mow.game.td;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import de.thm.mow.game.td.framework.GameGraphics;
import de.thm.mow.game.td.framework.Screen;
import de.thm.mow.game.td.model.InfoUiElement;
import de.thm.mow.game.td.model.InfoUiElement.InfoType;
import de.thm.mow.game.td.model.Stats;
import de.thm.mow.game.td.model.Tower;
import de.thm.mow.game.td.model.Tower.TowerType;
import de.thm.mow.game.td.model.UiElement;
import de.thm.mow.game.td.model.UiElement.UiType;
import de.thm.mow.game.td.model.Unit.UnitType;
import de.thm.mow.game.td.screens.GameScreen;
import de.thm.mow.game.td.screens.LevelSelectScreen;
import de.thm.mow.game.td.screens.MainMenuScreen;

public class UiManager {
	private AndroidGame mGame;

	private ArrayList<UiElement> mUiElements;
	private RessourceManager mRessourceManager;
	private Player mPlayer;

	public UiManager(AndroidGame game, RessourceManager ressourceManager, Player p) {
		mGame = game;
		mRessourceManager = ressourceManager;
		mUiElements = new ArrayList<UiElement>();
		mPlayer = p;

		if (mPlayer.getPlayerId() == Player.LOCAL_PLAYER) {
			UiElement res = new UiElement(800 - 320, 0, 320, 64, UiType.UI_RESSOURCE);
			res.mIsShowing = true;
			mUiElements.add(res);

			UiElement towerui = new UiElement(0, 0, 256, 256, UiType.UI_TOWER_BUILD);
			mUiElements.add(towerui);

			UiElement towerupgradeui = new UiElement(0, 0, 256, 256, UiType.UI_TOWER_UPGRADE);
			mUiElements.add(towerupgradeui);

			UiElement towerinfoui = new InfoUiElement(0, 0, 256, 256, UiType.UI_TOWER_INFO);
			mUiElements.add(towerinfoui);
			
			UiElement generalinfoui = new InfoUiElement(0, 0, 256, 256, UiType.UI_GENERAL);
			mUiElements.add(generalinfoui);
			
			UiElement towersellui = new InfoUiElement(0, 0, 256, 256, UiType.UI_TOWER_SELL);
			mUiElements.add(towersellui);

			UiElement spGameWon = new UiElement(100, 80, 600, 400, UiType.UI_GAME_SP_WON);
			mUiElements.add(spGameWon);

			UiElement spGameLost = new UiElement(100, 80, 600, 400, UiType.UI_GAME_SP_LOST);
			mUiElements.add(spGameLost);

			UiElement mpGameWon = new UiElement(100, 80, 600, 400, UiType.UI_GAME_MP_WON);
			mUiElements.add(mpGameWon);
			
			UiElement mpGameLost = new UiElement(100, 80, 600, 400, UiType.UI_GAME_MP_LOST);
			mUiElements.add(mpGameLost);

			UiElement gamePaused = new UiElement(100, 80, 600, 400, UiType.UI_GAME_PAUSED);
			mUiElements.add(gamePaused);	
			
		} else {
			UiElement enemyLife = new UiElement(0, 0, 256, 64, UiType.UI_RESSOURCE_ENEMY);
			enemyLife.mIsShowing = true;
			mUiElements.add(enemyLife);
		}
		


	}

	public void update(float deltaTime) {

	}

	public void showTowerBuildUi(float x, float y) {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_TOWER_BUILD) {
				if (e.x == x && e.y == y) {
					e.reset();
				} else {
					e.mIsShowing = true;
					e.x = x;
					e.y = y;
				}
				break;
			}
		}
	}

	public void showTowerUpgradeUi(float x, float y) {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_TOWER_UPGRADE) {
				if (e.x == x && e.y == y) {
					e.reset();
				} else {
					e.mIsShowing = true;
					e.x = x;
					e.y = y;
				}
				break;
			}
		}
	}

	public void showSPGameWonUi() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_SP_WON) {
				e.mIsShowing = true;
				break;
			}
		}
	}

	public void showSPGameLostUi() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_SP_LOST) {
				e.mIsShowing = true;
				break;
			}
		}
	}
	
	public void showMPGameWonUi() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_MP_WON) {
				e.mIsShowing = true;
				break;
			}
		}
	}
	
	public void showMPGameLostUi() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_MP_LOST) {
				e.mIsShowing = true;
				break;
			}
		}
	}

	public void showGamePausedUi() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_PAUSED) {
				e.mIsShowing = true;
				break;
			}
		}
	}

	public boolean isGamePausedUiShowing() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_PAUSED) {
				return e.mIsShowing;
			}
		}
		
		return false;
	}

	public boolean isGameMenuShowing() {
		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GAME_PAUSED) {
				return e.mIsShowing;
			}
			
			switch (e.type) {
			case UI_GAME_MP_LOST:
			case UI_GAME_MP_WON:
			case UI_GAME_PAUSED:
			case UI_GAME_SP_LOST:
			case UI_GAME_SP_WON:
				if (e.mIsShowing) {
					return true;
				}
				
				break;

			default:
				break;
			}
		}
		
		return false;
	}

	public void showTowerInfoUi(float x, float y, InfoType type) {
		final float newx = x + 192f;
		final float newy = y;// + 192f;

		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_TOWER_INFO) {
				((InfoUiElement) e).info = type;
				if (e.x == newx && e.y == newy) {
					e.reset();
				} else {
					e.mIsShowing = true;
					e.x = newx;
					e.y = newy;
				}
				break;
			}
		}
	}
	
	public void showTowerSellUi(float x, float y, InfoType type) {
		final float newx = x + 192f;
		final float newy = y;// + 192f;

		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_TOWER_SELL) {
				((InfoUiElement) e).info = type;
				if (e.x == newx && e.y == newy) {
					e.reset();
				} else {
					e.mIsShowing = true;
					e.x = newx;
					e.y = newy;
				}
				break;
			}
		}
	}
	
	public void showGeneralInfoUi(float x, float y, InfoType type) {
		final float newx = x + 192f;
		final float newy = y;// + 192f;

		for (UiElement e : mUiElements) {
			if (e.type == UiType.UI_GENERAL) {
				((InfoUiElement) e).info = type;
				if (e.x == newx && e.y == newy) {
					e.reset();
				} else {
					e.mIsShowing = true;
					e.x = newx;
					e.y = newy;
				}
				break;
			}
		}
	}
	

	public void draw(float deltaTime) {
		GameGraphics g = mGame.getGraphics();
		Screen s = mGame.getCurrentScreen();

		final int oX = s.offsetX;
		final int oY = s.offsetY;

		for (UiElement e : mUiElements) {
			if (e.mIsShowing) {
				switch (e.type) {
				case UI_RESSOURCE:

					g.drawBitmap(Asset.ressource_window, e.x, e.y);
					g.drawTextStandard(mRessourceManager.getStringCash(), 600, 40, Color.BLACK);
					g.drawTextStandard("" + mRessourceManager.getLifes(), 747, 40, Color.BLACK);
					break;
				case UI_TOWER_BUILD:
					g.drawBitmap(Asset.ui_circle_empty, e.x - e.width / 2 - oX, e.y - e.height / 2 - oY);

					if (!mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_MG_I)) { // less than 100, can't buy anything
						g.drawBitmap(Asset.towerlightning1Disabled, e.x - oX + e.width / 8, e.y - oY - e.height / 8); // not lightning
						g.drawBitmap(Asset.towerice1Disabled, e.x - oX - e.width / 8, e.y - oY + (e.height / 8)); // notice
						g.drawBitmap(Asset.towerflame1Disabled, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8); // not flame
						g.drawBitmap(Asset.towerbullet1Disabled, e.x - oX - e.width / 8, e.y - oY - 3 * (e.height / 8)); // not mg

					} else if (!mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_FLAME_I)) { // less than 120, only mg
						g.drawBitmap(Asset.towerlightning1Disabled, e.x - oX + e.width / 8, e.y - oY - e.height / 8); // not lightning
						g.drawBitmap(Asset.towerice1Disabled, e.x - oX - e.width / 8, e.y - oY + (e.height / 8)); // not ice
						g.drawBitmap(Asset.towerflame1Disabled, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8); // not  flame
						g.drawBitmap(Asset.towerbullet1, e.x - oX - e.width / 8, e.y - oY - 3 * (e.height / 8)); // mg

					} else if (!mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_ICE_I)) {// less than 150, only flame/mg
						g.drawBitmap(Asset.towerlightning1Disabled, e.x - oX + e.width / 8, e.y - oY - e.height / 8); // not lightning
						g.drawBitmap(Asset.towerice1Disabled, e.x - oX - e.width / 8, e.y - oY + (e.height / 8)); // not ice
						g.drawBitmap(Asset.towerflame1, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8); // flame
						g.drawBitmap(Asset.towerbullet1, e.x - oX - e.width / 8, e.y - oY - 3 * (e.height / 8)); // mg

					} else if (!mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_LIGHTNING_I)) { // less than 250 everything but lightning
						g.drawBitmap(Asset.towerlightning1Disabled, e.x - oX + e.width / 8, e.y - oY - e.height / 8); // not lightning
						g.drawBitmap(Asset.towerice1, e.x - oX - e.width / 8, e.y - oY + (e.height / 8)); // ice
						g.drawBitmap(Asset.towerflame1, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8); // flame
						g.drawBitmap(Asset.towerbullet1, e.x - oX - e.width / 8, e.y - oY - 3 * (e.height / 8)); // mg

					} else { // more than 250
						g.drawBitmap(Asset.towerlightning1, e.x - oX + e.width / 8, e.y - oY - e.height / 8); // lightning
						g.drawBitmap(Asset.towerice1, e.x - oX - e.width / 8, e.y - oY + (e.height / 8)); // ice
						g.drawBitmap(Asset.towerflame1, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8); // flame
						g.drawBitmap(Asset.towerbullet1, e.x - oX - e.width / 8, e.y - oY - 3 * (e.height / 8)); // mg
					}

					break;
				case UI_TOWER_UPGRADE:
					// TODO: Schoener machen
					MapManager map = mPlayer.getMapManager();
					TowerManager towermanager = mPlayer.getTowerManager();

					final int towerId = map.getTowerId(e.x, e.y);
					if (towerId != -1) {
						TowerType type = towermanager.getTowerType(towerId);

						g.drawBitmap(Asset.ui_circle_empty, e.x - e.width / 2 - oX, e.y - e.height / 2 - oY);

						int upgradeCost = 999999;
						switch (type) {
						case TOWER_FLAME_1:
							upgradeCost = Stats.COST_TOWER_FLAME_II;
							break;
						case TOWER_FLAME_2:
							upgradeCost = Stats.COST_TOWER_FLAME_III;
							break;
						case TOWER_ICE_1:
							upgradeCost = Stats.COST_TOWER_ICE_II;
							break;
						case TOWER_ICE_2:
							upgradeCost = Stats.COST_TOWER_ICE_III;
							break;
						case TOWER_MG_1:
							upgradeCost = Stats.COST_TOWER_MG_II;
							break;
						case TOWER_MG_2:
							upgradeCost = Stats.COST_TOWER_MG_III;
							break;
						case TOWER_LIGHTNING_1:
							upgradeCost = Stats.COST_TOWER_LIGHTNING_II;
							break;
						case TOWER_LIGHTNING_2:
							upgradeCost = Stats.COST_TOWER_LIGHTNING_III;
							break;

						default:
							break;
						}

						if (mRessourceManager.hasEnoughMoneyFor(upgradeCost)) {
							g.drawBitmap(Asset.ui_icon_upgrade, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8);
						} else {
							g.drawBitmap(Asset.ui_icon_upgrade_disabled, e.x - oX - 3 * e.width / 8, e.y - oY - e.height / 8);
						}

						g.drawBitmap(Asset.ui_icon_delete_sell, e.x - oX + e.width / 8, e.y - oY - e.height / 8);
					}
					break;
				case UI_TOWER_INFO:
					final float left = 336 - e.width / 2;
					final float top = 240 - e.height / 2;
					g.drawBitmap(Asset.ui_tower_info, left, top);

					if (((InfoUiElement) e).info != null) {

						String name = "";
						String cost = "";
						String damage = "";
						String reload = "";
						String range = "";
						//String reward = "";

						switch (((InfoUiElement) e).info) {
						case BUILD_TOWER_MG_I:
							name = "MG Tower";
							cost += Stats.COST_TOWER_MG_I;
							damage += Stats.DAMAGE_TOWER_MG_I;
							reload += Stats.RELOAD_TOWER_MG_I + " ms";
							range += Stats.RANGE_TOWER_MG_I;
							g.drawBitmap(Asset.towerbullet1, left + 280, top + 30);

							break;
						case BUILD_TOWER_FLAME_I:
							name = "Flame Tower";
							cost += Stats.COST_TOWER_FLAME_I;
							damage += Stats.DAMAGE_TOWER_FLAME_I;
							reload += Stats.RELOAD_TOWER_FLAME_I + " ms";
							range += Stats.RANGE_TOWER_FLAME_I;
							g.drawBitmap(Asset.towerflame1, left + 280, top + 30);

							break;
						case BUILD_TOWER_ICE_I:
							name = "Ice Tower";
							cost += Stats.COST_TOWER_ICE_I;
							damage += Stats.DAMAGE_TOWER_ICE_I;
							reload += Stats.RELOAD_TOWER_ICE_I + " ms";
							range += Stats.RANGE_TOWER_ICE_I;
							g.drawBitmap(Asset.towerice1, left + 280, top + 30);

							break;
						case BUILD_TOWER_LIGHTNING_I:
							name = "Lightning Tower";
							cost += Stats.COST_TOWER_LIGHTNING_I;
							damage += Stats.DAMAGE_TOWER_LIGHTNING_I;
							reload += Stats.RELOAD_TOWER_LIGHTNING_I + " ms";
							range += Stats.RANGE_TOWER_LIGHTNING_I;
							g.drawBitmap(Asset.towerlightning1, left + 280, top + 30);

							break;
						case UPGRADE_MG:
							name = "Upgrade";
							cost += Stats.COST_TOWER_MG_II;
							damage += Stats.DAMAGE_TOWER_MG_II;
							reload += Stats.RELOAD_TOWER_MG_II + " ms";
							range += Stats.RANGE_TOWER_MG_II;
							g.drawBitmap(Asset.towerbullet2, left + 280, top + 30);
							break;
						case UPGRADE_FLAME:
							name = "Upgrade";
							cost += Stats.COST_TOWER_FLAME_II;
							damage += Stats.DAMAGE_TOWER_FLAME_II;
							reload += Stats.RELOAD_TOWER_FLAME_II + " ms";
							range += Stats.RANGE_TOWER_FLAME_II;
							g.drawBitmap(Asset.towerflame2, left + 280, top + 30);
							break;
						case UPGRADE_ICE:
							name = "Upgrade";
							cost += Stats.COST_TOWER_ICE_II;
							damage += Stats.DAMAGE_TOWER_ICE_II;
							reload += Stats.RELOAD_TOWER_ICE_II + " ms";
							range += Stats.RANGE_TOWER_ICE_II;
							g.drawBitmap(Asset.towerice2, left + 280, top + 30);
							break;
						case UPGRADE_LIGHTNING:
							name = "Upgrade";
							cost += Stats.COST_TOWER_LIGHTNING_II;
							damage += Stats.DAMAGE_TOWER_LIGHTNING_II;
							reload += Stats.RELOAD_TOWER_LIGHTNING_II + " ms";
							range += Stats.RANGE_TOWER_LIGHTNING_II;
							g.drawBitmap(Asset.towerlightning2, left + 280, top + 30);
							break;
						case UPGRADE_MG_2:
							name = "Upgrade";
							cost += Stats.COST_TOWER_MG_III;
							damage += Stats.DAMAGE_TOWER_MG_III;
							reload += Stats.RELOAD_TOWER_MG_III + " ms";
							range += Stats.RANGE_TOWER_MG_III;
							g.drawBitmap(Asset.towerbullet3, left + 280, top + 30);
							break;
						case UPGRADE_FLAME_2:
							name = "Upgrade";
							cost += Stats.COST_TOWER_FLAME_III;
							damage += Stats.DAMAGE_TOWER_FLAME_III;
							reload += Stats.RELOAD_TOWER_FLAME_III + " ms";
							range += Stats.RANGE_TOWER_FLAME_III;
							g.drawBitmap(Asset.towerflame3, left + 280, top + 30);
							break;
						case UPGRADE_ICE_2:
							name = "Upgrade";
							cost += Stats.COST_TOWER_ICE_III;
							damage += Stats.DAMAGE_TOWER_ICE_III;
							reload += Stats.RELOAD_TOWER_ICE_III + " ms";
							range += Stats.RANGE_TOWER_ICE_III;
							g.drawBitmap(Asset.towerice3, left + 280, top + 30);
							break;
						case UPGRADE_LIGHTNING_2:
							name = "Upgrade";
							cost += Stats.COST_TOWER_LIGHTNING_III;
							damage += Stats.DAMAGE_TOWER_LIGHTNING_III;
							reload += Stats.RELOAD_TOWER_LIGHTNING_III + " ms";
							range += Stats.RANGE_TOWER_LIGHTNING_III;
							g.drawBitmap(Asset.towerlightning3, left + 280, top + 30);
							break;
						default:
							break;
						}

						// draw information on info ui window
						g.drawTextStandard(name, left + 40, top + 70, Color.WHITE);
						g.drawText(cost, left + 170, top + 125, Color.WHITE, 20);
						g.drawText(damage, left + 170, top + 158, Color.WHITE, 20);
						g.drawText(reload, left + 170, top + 191, Color.WHITE, 20);
						g.drawText(range, left + 170, top + 224, Color.WHITE, 20);
					}
	
					break;
				case UI_GAME_SP_LOST:
					g.drawBitmap(Asset.ui_game_over, 400 - e.width / 2, 240 - e.height / 2);
					break;
				case UI_GAME_SP_WON:
					g.drawBitmap(Asset.ui_game_win, 400 - e.width / 2, 240 - e.height / 2);
					break;
				case UI_GAME_MP_LOST:
					g.drawBitmap(Asset.ui_multiplayer_game_lose, 400 - e.width / 2, 240 - e.height / 2);
					break;
				case UI_GAME_MP_WON:
					g.drawBitmap(Asset.ui_multiplayer_game_survived, 400 - e.width / 2, 240 - e.height / 2);
					break;
				case UI_GAME_PAUSED:
					g.drawBitmap(Asset.ui_game_paused, 400 - e.width / 2, 240 - e.height / 2);
					break;
				case UI_RESSOURCE_ENEMY:
					g.drawBitmap(Asset.ui_multiplayer_ressource_enemy, e.x, e.y);
					g.drawTextStandard("" + mRessourceManager.getLifes(), 203, 40, Color.BLACK);
					break;
				case UI_GENERAL:
					final float left3 = 336 - e.width / 2;
					final float top3 = 240 - e.height / 2;
					g.drawBitmap(Asset.ui_general_info, left3, top3);
					g.drawTextStandard("Upgrade not possible", left3+45, top3+70, Color.WHITE);
					g.drawText("Tower already fully upgraded", left3+40, top3+160, Color.WHITE, 20);
					break;
				case UI_TOWER_SELL:
					final float left2 = 336 - e.width / 2;
					final float top2 = 240 - e.height / 2;
					g.drawBitmap(Asset.ui_general_info, left2, top2);
					g.drawTextStandard("Sell tower", left2+45, top2+70, Color.WHITE);
					g.drawText("Get 50% of its cost back", left2+40, top2+160, Color.WHITE, 20);
					break;
				case UI_MP_MENU_SEND_UNITS:
					g.drawBitmap(Asset.ui_multiplayer_send_units, e.x, e.y);
					
					if(e.x != -695 && e.mIsShowing)
					{
						g.drawText(""+Stats.COST_UNIT_SOLDIER, 130, 480-128+50, Color.BLACK, 20);
						g.drawText(""+Stats.HEALTH_UNIT_SOLDIER, 143, 480-128+75, Color.BLACK, 20);
						g.drawText(""+Stats.STRENGTH_UNIT_SOLDIER, 163, 480-128+100, Color.BLACK, 20);
						
						g.drawText(""+Stats.COST_UNIT_TANK, 315, 480-128+50, Color.BLACK, 20);
						g.drawText(""+Stats.HEALTH_UNIT_TANK, 332, 480-128+75, Color.BLACK, 20);
						g.drawText(""+Stats.STRENGTH_UNIT_TANK, 352, 480-128+100, Color.BLACK, 20);
						
						g.drawText(""+Stats.COST_UNIT_BOMB, 500, 480-128+50, Color.BLACK, 20);
						g.drawText(""+Stats.HEALTH_UNIT_BOMB, 517, 480-128+75, Color.BLACK, 20);
						g.drawText(""+Stats.STRENGTH_UNIT_BOMB, 538, 480-128+100, Color.BLACK, 20);	
					}
					break;		
				}
			}
		}
	}

	/**
	 * 
	 * @param x
	 *            coord of the click
	 * @param y
	 *            coord of the click
	 * @return true if Ui was clicked, false if not so that other "handlers" can
	 *         check if they were meant to do something
	 */
	public boolean clickedAt(float x, float y) {
		boolean wasClicked = false;

		Screen s = mGame.getCurrentScreen();

		final int oX = s.offsetX;
		final int oY = s.offsetY;

		for (UiElement e : mUiElements) {
			switch (e.type) {
			case UI_RESSOURCE:
				//if (e.clickedAtArea(x - oX, y - oY)) {
				if(x - oX > e.x && x - oX < e.x+64 && y - oY > 0 && y - oY < 64){
					if (!isGameMenuShowing()) {
						showGamePausedUi();
						mGame.getCurrentScreen().pause();
					}
					wasClicked = true;
				}
				break;
			case UI_TOWER_BUILD:
				if (e.mIsShowing) {
					MapManager manager = mPlayer.getMapManager();
					if (x > e.x - 32 && x < e.x + 32 && y < e.y - 32 && y > e.y - 96) { // MG
						if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_MG_I)) {
							manager.buildTower(TowerType.TOWER_MG_1, manager.getTileIndex(e.x, e.y));
							mRessourceManager.removeCash(Stats.COST_TOWER_MG_I);
							mGame.getNetwork().sendTowerBuildDataMessage(manager.getTileIndex(e.x, e.y), TowerType.TOWER_MG_1.getType());
						}
					} else if (x > e.x - 32 && x < e.x + 32 && y > e.y + 32 && y < e.y + 96) { // ICE
						if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_ICE_I)) {
							manager.buildTower(TowerType.TOWER_ICE_1, manager.getTileIndex(e.x, e.y));
							mRessourceManager.removeCash(Stats.COST_TOWER_ICE_I);
							mGame.getNetwork().sendTowerBuildDataMessage(manager.getTileIndex(e.x, e.y), TowerType.TOWER_ICE_1.getType());
						}
					} else if (x > e.x - 96 && x < e.x - 32 && y > e.y - 32 && y < e.y + 32) { // FLAME
						if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_FLAME_I)) {
							manager.buildTower(TowerType.TOWER_FLAME_1, manager.getTileIndex(e.x, e.y));
							mRessourceManager.removeCash(Stats.COST_TOWER_FLAME_I);
							mGame.getNetwork().sendTowerBuildDataMessage(manager.getTileIndex(e.x, e.y), TowerType.TOWER_FLAME_1.getType());
						}
					} else if (x > e.x + 32 && x < e.x + 96 && y > e.y - 32 && y < e.y + 32) { // LIGHTNING
						if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_TOWER_LIGHTNING_I)) {
							manager.buildTower(TowerType.TOWER_LIGHTNING_1, manager.getTileIndex(e.x, e.y));
							mRessourceManager.removeCash(Stats.COST_TOWER_LIGHTNING_I);
							mGame.getNetwork().sendTowerBuildDataMessage(manager.getTileIndex(e.x, e.y), TowerType.TOWER_LIGHTNING_1.getType());
						}
					}
					e.reset();
					wasClicked = true;
				}
			case UI_TOWER_UPGRADE:
				if (e.mIsShowing) {

					MapManager map = mPlayer.getMapManager();
					TowerManager towermanager = mPlayer.getTowerManager();

					if (x > e.x - 96 && x < e.x - 32 && y > e.y - 32 && y < e.y + 32) { // Upgrade
						final int towerId = map.getTowerId(e.x, e.y);
						TowerType type = towermanager.getTowerType(towerId);

						if (type == TowerType.TOWER_FLAME_3 || type == TowerType.TOWER_ICE_3 || type == TowerType.TOWER_LIGHTNING_3 || type == TowerType.TOWER_MG_3) {
							// can't upgrade tower lvl 3
							e.reset();
							wasClicked = true;
							break;
						}

						int upgradeCost = 999999;
						switch (type) {
						case TOWER_FLAME_1:
							upgradeCost = Stats.COST_TOWER_FLAME_II;
							break;
						case TOWER_FLAME_2:
							upgradeCost = Stats.COST_TOWER_FLAME_III;
							break;
						case TOWER_ICE_1:
							upgradeCost = Stats.COST_TOWER_ICE_II;
							break;
						case TOWER_ICE_2:
							upgradeCost = Stats.COST_TOWER_ICE_III;
							break;
						case TOWER_MG_1:
							upgradeCost = Stats.COST_TOWER_MG_II;
							break;
						case TOWER_MG_2:
							upgradeCost = Stats.COST_TOWER_MG_III;
							break;
						case TOWER_LIGHTNING_1:
							upgradeCost = Stats.COST_TOWER_LIGHTNING_II;
							break;
						case TOWER_LIGHTNING_2:
							upgradeCost = Stats.COST_TOWER_LIGHTNING_III;
							break;

						default:
							break;
						}

						if (mRessourceManager.hasEnoughMoneyFor(upgradeCost)) {
							towermanager.upgradeTower(towerId);
							mRessourceManager.removeCash(upgradeCost);
							mGame.getNetwork().sendTowerUpgradeDataMessage(towerId);
						}
					} else if (x > e.x + 32 && x < e.x + 96 && y > e.y - 32 && y < e.y + 32) { // Sell
						final int id = map.getTowerId(e.x, e.y);
						int cashback = 0;
						Tower t = towermanager.getTower(id);
						TowerType type = towermanager.getTowerType(id);
						int tileIndex = map.getTileIndex(e.x, e.y);
						map.sellTower(t, tileIndex);
						mGame.getNetwork().sendTowerSellDataMessage(id, tileIndex);
						
						
						switch (type) {
						case TOWER_FLAME_1:
							cashback = Stats.COST_TOWER_FLAME_I/2;
							break;
						case TOWER_FLAME_2:
							cashback = (Stats.COST_TOWER_FLAME_I + Stats.COST_TOWER_FLAME_II)/2;
							break;
						case TOWER_FLAME_3:
							cashback = (Stats.COST_TOWER_FLAME_I + Stats.COST_TOWER_FLAME_II + Stats.COST_TOWER_FLAME_III)/2;
							break;
						case TOWER_ICE_1:
							cashback = Stats.COST_TOWER_ICE_I/2;
							break;
						case TOWER_ICE_2:
							cashback = (Stats.COST_TOWER_ICE_I + Stats.COST_TOWER_ICE_II)/2;
							break;
						case TOWER_ICE_3:
							cashback = (Stats.COST_TOWER_ICE_I + Stats.COST_TOWER_ICE_II + Stats.COST_TOWER_ICE_III)/2;
							break;
						case TOWER_MG_1:
							cashback = Stats.COST_TOWER_MG_I/2;
							break;
						case TOWER_MG_2:
							cashback = (Stats.COST_TOWER_MG_I + Stats.COST_TOWER_MG_II)/2;
							break;
						case TOWER_MG_3:
							cashback = (Stats.COST_TOWER_MG_I + Stats.COST_TOWER_MG_II + Stats.COST_TOWER_MG_III)/2;
							break;
						case TOWER_LIGHTNING_1:
							cashback = Stats.COST_TOWER_LIGHTNING_I/2;
							break;
						case TOWER_LIGHTNING_2:
							cashback = (Stats.COST_TOWER_LIGHTNING_I + Stats.COST_TOWER_LIGHTNING_II)/2;
							break;
						case TOWER_LIGHTNING_3:
							cashback = (Stats.COST_TOWER_LIGHTNING_I + Stats.COST_TOWER_LIGHTNING_II + Stats.COST_TOWER_LIGHTNING_III)/2;
							break;
						default:
							break;
						}						
						mRessourceManager.addCash(cashback);
					}
					e.reset();
					wasClicked = true;
				}
				break;
			case UI_GENERAL:
				if (e.mIsShowing) {
					e.reset();
					wasClicked = true;
				}
				break;
			case UI_TOWER_INFO:
				if (e.mIsShowing) {
					e.reset();
					wasClicked = true;
				}
				break;
			case UI_TOWER_SELL:
				if (e.mIsShowing) {
					e.reset();
					wasClicked = true;
				}
				break;
			case UI_GAME_PAUSED:
				if (e.mIsShowing) {
					if (x - oX > e.x + 25 && x - oX < e.x + e.width/2 - 10 && y - oY > e.y + 130 && y - oY < e.y + e.height - 30) {
						e.mIsShowing = false;
						mGame.getCurrentScreen().resume();
					} else if (x - oX  > e.x + e.width/2 + 10 && x - oX < e.x + e.width - 25 && y - oY  > e.y + 130 && y - oY  < e.y + e.height - 30) {
						mGame.setScreen(new MainMenuScreen(mGame));
					}
					wasClicked = true;
				}
				break;
			case UI_GAME_SP_WON:
				if (e.mIsShowing) {
					if (x - oX > e.x + 25 && x - oX < e.x + e.width/2 - 10 && y - oY > e.y + 130 && y - oY < e.y + e.height - 30) {
						Bundle b = new Bundle();
						b.putInt(LevelSelectScreen.MAP_SELECTED, mPlayer.getMapId());
						mGame.setScreen(new GameScreen(mGame, false, b));
					} else if (x - oX  > e.x + e.width/2 + 10 && x - oX < e.x + e.width - 25 && y - oY  > e.y + 130 && y - oY  < e.y + e.height - 30) {
						mGame.setScreen(new MainMenuScreen(mGame));
					}
					wasClicked = true;
				}
				break;
			case UI_GAME_SP_LOST:
				if (e.mIsShowing) {
					if (x - oX > e.x + 25 && x - oX < e.x + e.width/2 - 10 && y - oY > e.y + 130 && y - oY < e.y + e.height - 30) {
						Bundle b = new Bundle();
						b.putInt(LevelSelectScreen.MAP_SELECTED, mPlayer.getMapId());
						mGame.setScreen(new GameScreen(mGame, false, b));
					} else if (x - oX  > e.x + e.width/2 + 10 && x - oX < e.x + e.width - 25 && y - oY  > e.y + 130 && y - oY  < e.y + e.height - 30) {
						mGame.setScreen(new MainMenuScreen(mGame));
					}
					wasClicked = true;
				}
				break;
			case UI_GAME_MP_WON:
			case UI_GAME_MP_LOST:
				if (e.mIsShowing) {
					if (x - oX > e.x + 25 && x - oX < e.x + e.width - 25 && y - oY > e.y + 130 && y - oY < e.y + e.height - 30) {
						mGame.setScreen(new MainMenuScreen(mGame));
						wasClicked = true;
					}
				}
				break;
			case UI_MP_MENU_SEND_UNITS:
				if (!isGameMenuShowing() && e.mIsShowing) {
					if(x - oX  > 0 && x - oX  < 800 && y - oY > 480-128 && y - oY < 480){
						if(e.x == -695){ //menu is closed
							if(x - oX > 0 && x - oX < 105 && y - oY > 352 && y - oY < 480){
								//open menu
								wasClicked = true;
								e.x = 0;
							} else {
								wasClicked = false;
							}
						}
						else{ //menu is open
							if(x - oX > 695 && x - oX < 800 && y - oY > 352 && y - oY < 480){
								//close menue
								e.x = -695;
							}
							else if(x - oX > 392 && x - oX < 570 && y - oY > 480-128 && y - oY < 480){
								// send unit square
								if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_UNIT_BOMB)) {
									mGame.getNetwork().sendSendUnitMessage(UnitType.BOMB.getType());
									mRessourceManager.removeCash(Stats.COST_UNIT_BOMB);
									mPlayer.getManagerCallback().sendUnitsToAllButLocal(UnitType.BOMB);
								}
							}
							else if(x - oX > 202 && x - oX < 380 && y - oY > 480-128 && y - oY < 480){
								// send unit star
								if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_UNIT_TANK)) {
									mGame.getNetwork().sendSendUnitMessage(UnitType.TANK.getType());
									mRessourceManager.removeCash(Stats.COST_UNIT_TANK);
									mPlayer.getManagerCallback().sendUnitsToAllButLocal(UnitType.TANK);
								}
							}
							else if(x - oX > 12 && x - oX < 190 && y - oY > 480-128 && y - oY < 480){
								// send unit circle
								if (mRessourceManager.hasEnoughMoneyFor(Stats.COST_UNIT_SOLDIER)) {
									mGame.getNetwork().sendSendUnitMessage(UnitType.SOLDIER.getType());
									mRessourceManager.removeCash(Stats.COST_UNIT_SOLDIER);
									mPlayer.getManagerCallback().sendUnitsToAllButLocal(UnitType.SOLDIER);
								}
							}
							wasClicked = true;
						}
					}
					
				}

				break;
			default:
				break;
			}
		}
		return wasClicked;
	}

	public boolean longClickAt(float x, float y) {
		boolean wasClicked = false;

		Screen s = mGame.getCurrentScreen();

		final int oX = s.offsetX;
		final int oY = s.offsetY;

		for (UiElement e : mUiElements) {
			switch (e.type) {
			case UI_RESSOURCE:
				if (e.clickedAtArea(x - oX, y - oY)) {
					wasClicked = true;
				}
				break;
			case UI_TOWER_BUILD:
				if (e.mIsShowing) {
					// TODO: Needs to be done better!!!
					if (x > e.x - 32 && x < e.x + 32 && y < e.y - 32 && y > e.y - 96) { // MG Tower information
						showTowerInfoUi(x, y, InfoType.BUILD_TOWER_MG_I);
					} else if (x > e.x - 32 && x < e.x + 32 && y > e.y + 32 && y < e.y + 96) { // ICE Tower information
						showTowerInfoUi(x, y, InfoType.BUILD_TOWER_ICE_I);
					} else if (x > e.x - 96 && x < e.x - 32 && y > e.y - 32 && y < e.y + 32) { // FLAME Tower information
						showTowerInfoUi(x, y, InfoType.BUILD_TOWER_FLAME_I);
					} else if (x > e.x + 32 && x < e.x + 96 && y > e.y - 32 && y < e.y + 32) { // LIGHTNING Tower information
						showTowerInfoUi(x, y, InfoType.BUILD_TOWER_LIGHTNING_I);
					}
					e.reset();
					wasClicked = true;

				}
			case UI_TOWER_UPGRADE:
				if (e.mIsShowing) {
					if (x > e.x - 96 && x < e.x - 32 && y > e.y - 32 && y < e.y + 32) { // Upgrade information
						
						MapManager map = mPlayer.getMapManager();
						TowerManager towermanager = mPlayer.getTowerManager();

						final int towerId = map.getTowerId(e.x, e.y);
						TowerType type = towermanager.getTowerType(towerId);

						if (type == TowerType.TOWER_MG_1){
							showTowerInfoUi(x, y, InfoType.UPGRADE_MG);
						}
						if (type == TowerType.TOWER_MG_2){
							showTowerInfoUi(x, y, InfoType.UPGRADE_MG_2);
						}
						if (type == TowerType.TOWER_MG_3){
							showGeneralInfoUi(x, y, InfoType.GENERAL);
						}
						if (type == TowerType.TOWER_FLAME_1){
							showTowerInfoUi(x, y, InfoType.UPGRADE_FLAME);
						}
						if (type == TowerType.TOWER_FLAME_2){
							showTowerInfoUi(x, y, InfoType.UPGRADE_FLAME_2);
						}
						if (type == TowerType.TOWER_FLAME_3){
							showGeneralInfoUi(x, y, InfoType.GENERAL);
						}
						if (type == TowerType.TOWER_ICE_1){
							showTowerInfoUi(x, y, InfoType.UPGRADE_ICE);
						}
						if (type == TowerType.TOWER_ICE_2){
							showTowerInfoUi(x, y, InfoType.UPGRADE_ICE_2);
						}
						if (type == TowerType.TOWER_ICE_3){
							showGeneralInfoUi(x, y, InfoType.GENERAL);
						}
						if (type == TowerType.TOWER_LIGHTNING_1){
							showTowerInfoUi(x, y, InfoType.UPGRADE_LIGHTNING);
						}
						if (type == TowerType.TOWER_LIGHTNING_2){
							showTowerInfoUi(x, y, InfoType.UPGRADE_LIGHTNING_2);
						}
						if (type == TowerType.TOWER_LIGHTNING_3){
							showGeneralInfoUi(x, y, InfoType.GENERAL);
						}
						
					} else if (x > e.x + 32 && x < e.x + 96 && y > e.y - 32 && y < e.y + 32) { // Sell information
						showTowerSellUi(x, y, InfoType.SELL_TOWER);
					}
					e.reset();
					wasClicked = true;
				}
				break;
			default:
				break;
			}
		}
		return wasClicked;
	}
	
	public void addMultiPlayerMenu(){
		UiElement menuUnitSend = new UiElement(-695, 480-128, 800, 128, UiType.UI_MP_MENU_SEND_UNITS);
		mUiElements.add(6, menuUnitSend);
		menuUnitSend.mIsShowing = true;
	}

	public void disableSendMenu() {
		for (UiElement e: mUiElements) {
			if (e.type == UiType.UI_MP_MENU_SEND_UNITS) {
				e.mIsShowing = false;
			}
		}
	}
	
}
